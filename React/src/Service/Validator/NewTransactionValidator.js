class TransactionValidator {
    validate(jsondata) {
        let result = true
        if (!jsondata.customer) {
            alert('Cart or Customer Can not be empty')
            result=false
        }

        if (jsondata.cart.length ===0) {
            alert('Cart Can not be empty')
            result=false
        }

        return result
    }
}
export default new TransactionValidator()