class PromoValidator {
    discount(jsondata) {
        let result = true
        if (jsondata.discount<0) {
            alert('Discount !=0')
            result=false
        }

        if (jsondata.forPurchase<0) {
            alert('Minimum Purchase must be higher than 0')
            result=false
        }

        if (jsondata.promoProduct.length<=0 || jsondata.description==null ||!jsondata.startDate ||!jsondata.endDate) {
            alert('Please Fill Out All requiered Fields')
            result=false
        }
        return result
    }

    freeGood(jsondata) {
        let result = true
        if (jsondata.forPurchase<0 ) {
            alert('Minimum Purchase must be higher than 0')
            result=false
        }

        if (!jsondata.freeGood ) {
            alert('Please Select Free Good')
            result=false
        }

        if (jsondata.quantity<=0 && jsondata.quantity!=null) {
            alert('Quantity must be higher than 0')
            result=false
        }

        if (jsondata.promoProduct.length<=0 ||jsondata.description==null ||!jsondata.startDate ||!jsondata.endDate) {
            alert('Please Fill Out All requiered Fields')
            result=false
        }

        return result
    }
}
export default new PromoValidator()