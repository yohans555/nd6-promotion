class OtherService {
    getPromoProduct(listproduct, listId){
        let promoProduct = []
        listId.forEach(element => {
            listproduct.forEach(res => {
                if (res.id===element.productId) {
                    let tempProduct = {
                        productId:res.id ,
                        name: res.name,
                        price:res.price, 
                    }
                    promoProduct.push(tempProduct);
                }
            });
        });
       return promoProduct;
    }

    filteredData(rows){
        let a = rows.filter((row)=>row.id.indexOf(this.state.q)>-1 )
        let b = a.map((element) => {
          return {...element, promoProduct: element.promoProduct.filter((e) => e.productId.toString().indexOf(this.state.q2)>-1)}
        })
        let c = b.filter(row=> row.promoProduct.length>0)
        return c
      }

    CancelPromo(rows){
        let a = rows.filter((res)=>res.price!==0)
        for( var k = 0; k < a.length; ++k ) {
            a[k]["discount"] = 0 ;
        }
        return a
    }
    CountTotal(rows){
        let total = 0;
        rows.forEach(element => {
            total += element.price*element.quantity
        });
        return total;
    }
}

export default new OtherService();
