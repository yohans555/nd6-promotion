import React, { Component } from 'react';
import {BrowserRouter, Route, Switch,Redirect} from 'react-router-dom'
import Navbar from './components/Navbar/Navbar'
import Promotion from './Pages/Promotion/Promotion'
import NewTransaction from './Pages/NewTransaction/NewTransaction';
import TransactionTable from './Pages/Transaction/Transaction';
import SignInSide from './Pages/Login/Login'
import Register from './Pages/Login/Register';

class App extends Component {
  render() {
    return (
       <BrowserRouter>
            <div className="App">
              <Navbar/>
                <Switch>
                    <SecureRoute path="/transaction">
                        <TransactionTable/>
                    </SecureRoute>
                    <SecureRoute path="/promotion">
                        <Promotion/>
                    </SecureRoute>
                    <SecureRoute path="/new-transaction">
                        <NewTransaction/>
                    </SecureRoute>
                    <SecureRoute path="/edit">
                        <NewTransaction type="edit"/>
                    </SecureRoute>
                    <Route exact path="/" component={SignInSide}/>
                    <Route exact path="/register" component={Register}/>
                  </Switch>
             </div>
       </BrowserRouter>
    );
  }
}

export default App;

function SecureRoute({ children, ...rest }) {
  return (
    <Route
      {...rest}
      render={({ location }) =>
        localStorage.getItem("user") !=null ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: "/",
              state: { from: location },
            }}
          />
        )
      }
    />
  );
}