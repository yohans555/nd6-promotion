import { ADD_TO_CART,REMOVE_ITEM,SUB_QUANTITY,ADD_QUANTITY,TEMP_DATA,SELECT_CUSTOMER} from '../actions/action-types/cart-actions'
import Service from '../../Service/Service'
import OtherService from '../../Service/OtherService'


let initState = {
    items:[],
    addedItems:[],
    customerList:[],
    selectedCustomer:'',
    PromoList:[],
    total: 0,
    tempData:{},
}
Service.getData("product").then((response)=>initState.items=response.data)
Service.getData("customer").then((response)=>initState.customerList =response.data)

const Reducer= (state = initState,action)=>{
    //ADD TO CART
    if(action.type === ADD_TO_CART){
         let addedItem = state.items.find(item=> item.productId === action.id)
          //check if the action id exists in the addedItems
         let existed_item= state.addedItems.find(item=> action.id === item.productId)
         if(existed_item)
         {
            addedItem.quantity += 1 
             return{
                ...state,
                 total: state.total + addedItem.price 
                  }
        }
         else{
            addedItem.quantity = 1;
            //calculating the total
            let newTotal = state.total + addedItem.price 
            
            return{
                ...state,
                addedItems: [...state.addedItems, addedItem],
                total : newTotal
            }
            
        }
    }
    if(action.type === REMOVE_ITEM){
        let itemToRemove= state.addedItems.find(item=> action.id === item.productId)
        let new_items = state.addedItems.filter(item=> action.id !== item.productId)
        
        //calculating the total
        let newTotal = state.total - (itemToRemove.price * itemToRemove.quantity )
        return{
            ...state,
            addedItems: new_items,
            total: newTotal
        }
    }

    // Inserting any item to temp data
    if(action.type=== TEMP_DATA){
        console.log(`object`, action.id)
          return{
              ...state,
              tempData: action.id,
              addedItems:OtherService.CancelPromo(action.id.transactionProduct) ,
              selectedCustomer: action.id.customer,
              total:OtherService.CountTotal(action.id.transactionProduct)
          }
    }

    // When Select Customer
    if(action.type=== SELECT_CUSTOMER){
          return{
              ...state,
              selectedCustomer: action.id
          }
    }
    
    //INSIDE CART COMPONENT
    if(action.type=== ADD_QUANTITY){
        let addedItem = state.addedItems.find(item=> item.productId === action.id)
          addedItem.quantity += 1 
          console.log(`object`, addedItem)
          let newTotal = state.total + addedItem.price
          return{
              ...state,
              total: newTotal
          }
    }
    if(action.type=== SUB_QUANTITY){  
        let addedItem = state.addedItems.find(item=> item.productId === action.id) 
        //if the qt == 0 then it should be removed
        if(addedItem.quantity === 1){
            let new_items = state.addedItems.filter(item=>item.productId !== action.id)
            let newTotal = state.total - addedItem.price
            return{
                ...state,
                addedItems: new_items,
                total: newTotal
            }
        }
        else {
            addedItem.quantity -= 1
            let newTotal = state.total - addedItem.price
            return{
                ...state,
                total: newTotal
            }
        }
        
    }
    
  else{
    return state
    }
    
}

export default Reducer
