import React, { Component } from "react";
import MUIDataTable from "mui-datatables";
import "./NewTransaction.css";
import { connect } from "react-redux";
import { addToCart, selectCustomer} from "../../Redux/actions/cartActions";
import Cart from "../../components/Cart/Cart";
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import AddShoppingCartIcon from '@material-ui/icons/AddShoppingCart';
import Badge from '@material-ui/core/Badge';
import IconButton from '@material-ui/core/IconButton';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';

class NewTransaction extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sidebar: false,
    };
  }
  handleAddToCart = (id) => {
    this.props.addToCart(id);
  };
  
  showSidebar = ()=>{
    this.setState({sidebar:!this.state.sidebar})
  }

  PropsView = () =>{
    switch (this.props.type) {
      case "edit":
        return (
        <>
        <h6>Document Number: <b>{this.props.tempData.id}</b>       Customer: <b>{this.props.tempData.customer.name}</b></h6>
        </>)
      default:  
        return (
          <Autocomplete
              onChange={(event, value) =>this.props.selectCustomer(value) }
              id="customer-select"
              options={this.props.customers}
              getOptionLabel={(option) => (option.id + '-'+option.name)}
              style={{ width: 300}}
              renderInput={(params) => <TextField {...params} label="Select Customer" variant="outlined" size="small"  />}
            />
        )
    }
  }
 
  render() {
    const columns = [
      {
        name: "productId",
        label: "ID",
        options: {
          filter: false,
          sort: false,
        },
      },
      {
        name: "name",
        label: "PRODUCT_NAME",
        options: {
          filter: false,
          sort: false,
        },
      },
      {
        name: "stock",
        label: "STOCK",
        options: {
          filter: false,
          sort: false,
        },
      },
      {
        name: "price",
        label: "PRICE",
        options: {
          filter: false,
          sort: false,
        },
      },
      {
        name: "",
        label: "ACTION",
        options: {
          filter: false,
          sort: false,
          customBodyRenderLite: (dataIndex) => {
            return (
              <IconButton size='small'  onClick={()=>{this.props.addToCart(this.props.products[dataIndex].productId);}}>
                  <AddShoppingCartIcon className='add-to-cart-icon'/>
              </IconButton>
              
            );
          },
        },
      },
    ];

    const options = {
      rowsPerPageOptions:[1,3,5,7],
      rowsPerPage: 5,
      customToolbar:()=> 
      <IconButton size='small' onClick={this.showSidebar} aria-label="cart">
        <Badge badgeContent={this.props.variant.length} color="secondary">
          <ShoppingCartIcon />
        </Badge>
      </IconButton>,
      isRowSelectable: () => false,
      selectableRowsHeader: false,
      setRowProps: (row, meta, rowIndex) => {
        if (rowIndex % 2 === 0) {
          return {
            style: { background: "wheat" },
          };
        }
      },
    };
    return (
      <div id="body">
        <div id="content">  
          <div id={this.state.sidebar ? 'half-table' : 'full-table'}>
            {this.PropsView()}
            <br></br>
            <MUIDataTable
              title={"Product List"}
              data={this.props.products}
              columns={columns}
              options={options}
            />
          </div>
            <div className={this.state.sidebar ? 'cart-active' : 'cart-hide'}>
              <Cart type={this.props.type}/>
            </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    products: state.items,
    customers:state.customerList,
    variant:state.addedItems,
    tempData: state.tempData
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    addToCart: (id) => {
      dispatch(addToCart(id));
    },
    selectCustomer: (id) => {
      dispatch(selectCustomer(id));
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NewTransaction);
