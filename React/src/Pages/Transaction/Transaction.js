import React, { Component } from "react";
import Service from "../../Service/Service";
import "./Transaction.css";
import MUIDataTable from "mui-datatables";
import CustomToolbarSelect from './CustomToolBar'
import DetailsTransaction from "../../components/DetailTable/DetailTransaction";
import { TablePagination, TableFooter } from "@material-ui/core";

class TransactionTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      q:'',
      q2:'',
      rowsPerPage:7,
      page:0, 
      items: [],
    };
    this.handleChangePage=this.handleChangePage.bind(this);
  }

  fetchItems(){
    Service.getPage('transaction?page='+this.state.page.toString()+'&elements='+this.state.rowsPerPage.toString()+'&id='+this.state.q,'').then(
      (res)=>{this.setState({ items: res.data })})
  }

  componentDidMount() {
    this.fetchItems()
  }

  handlechange(e){
    this.setState({q:e.target.value},()=>{
    this.fetchItems()
    })
  }

  handleChangePage = (event, newPage) => {
    this.setState({ page: newPage },()=>{
     this.fetchItems()
    });
  };

  handleChangeRowsPerPage = (event) => {
      this.setState({ rowsPerPage: +event.target.value });
      this.setState({ page: 0 },()=>{
        this.fetchItems()
      });
    };

  render() {
    const {items} = this.state
    const columns = [
      {
        name: "id",
        label: "DOCUMENT NUMBER",
        options: {
          filter: false,
          sort: false,
        },
      },
      {
        name: "date",
        label: "DOCUMENT DATE",
        options: {
          filter: false,
          sort: false,
        },
      },
      {
        name: "total",
        label: "TOTAL TRANSAKSI",
        options: {
          filter: true,
          sort: false,
        },
      },
      {
        name: "customer.name",
        label: "CUSTOMER",
        options: {
          filter: false,
          sort: false,
          customBodyRenderLite: (dataIndex) => {
            try {
              return this.state.items[dataIndex].customer.name;
            } catch (error) {
              return ''
            }
          }
        },
      },
    ]

    const options = {
      selectableRows:'single',
      pagination:false,
      search:false,
      selectableRowsOnClick:true,
      customToolbarSelect: (selectedRows) => <CustomToolbarSelect items={this.state.items} selectedRows={selectedRows} />,
      expandableRows: true, 
      renderExpandableRow: (rowData) => {
        const data = items[items.findIndex((response)=>{return response.id===rowData[0]})]
          return (
            <>
            <DetailsTransaction data={data}/>
            </>
          )
        
      },
      setRowProps: (row,meta,rowIndex) => { 
        if ((rowIndex%2 === 0)) {
          return {
            style: { background: "wheat" }
          };
        }
      },
    }
    return (
      <div id="body">
        <div id="contents">
          <br></br>
          
          <MUIDataTable
            title={
            <div className='search-bar'>
              <h6 className='heading-6'><b>Transaction History</b></h6> 
              <input type='text' value={this.state.q} placeholder='Search By Document' onChange={(e)=>this.handlechange(e)}/>
            </div>}
            data={items}
            columns={columns}
            options={options}
          />
          <TableFooter>
            <TablePagination
            rowsPerPageOptions={[10,7,5]}
            component="div"
            count={100}
            rowsPerPage={this.state.rowsPerPage}
            page={this.state.page}
            onChangePage={this.handleChangePage}
            onChangeRowsPerPage={this.handleChangeRowsPerPage}
            >
            </TablePagination>
          </TableFooter>
        </div>
      </div>
    );
  }
}
export default TransactionTable;
