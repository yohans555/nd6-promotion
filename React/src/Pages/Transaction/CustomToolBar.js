import React from "react";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import Service from "../../Service/Service";
import Example from "../../components/Print Invoice/Print";
import { Link } from "react-router-dom";
import { connect } from 'react-redux'
import { addTempData, selectCustomer} from '../../Redux/actions/cartActions'
import EditIcon from '@material-ui/icons/Edit';


class CustomToolbarSelect extends React.Component {
  //to remove the item completely
  handleClick = () => {   
    var yes = window.confirm("are you sure?");
    if(yes){ 
          Service.deleteData(
            "transaction",
            this.props.items[this.props.selectedRows.data[0].dataIndex].id
          );      
        window.location.reload()
      }
  }

   // handle edit
   handleTest = ()=>{
    this.props.addTempData( this.props.items[this.props.selectedRows.data[0].dataIndex]);
    localStorage.setItem('id',this.props.items[this.props.selectedRows.data[0].dataIndex].id)
  }
 

  render() {
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();
        let curdate = yyyy+'-'+ mm+'-'+dd
    return (
      <div className={"custom-toolbar-select"}>
        <IconButton
         disabled= {this.props.items[this.props.selectedRows.data[0].dataIndex].date===curdate?(false):(true)}
         tooltip="Edit"
         component={Link}
         to='/edit'
         onClick={this.handleTest}
         linkButton={true}>
          <EditIcon  className='edit-icon'/>
        </IconButton>
        
          <IconButton onClick={this.handleClick}>
            <DeleteIcon  className='delete-icon'/>
          </IconButton>
        <Example data={ this.props.items[this.props.selectedRows.data[0].dataIndex]} />
      </div>
    );
  }
}

const mapStateToProps = (state)=>{
  return{
    a: state.addedItems,
  }
}
const mapDispatchToProps = (dispatch)=>{
  return{
      addTempData:(id)=>{dispatch(addTempData(id))},
      selectCustomer: (id) => {
        dispatch(selectCustomer(id));
      },
  }
}
export default (connect(mapStateToProps,mapDispatchToProps)(CustomToolbarSelect));