import React from 'react';
import {Button,Link,Paper,Grid,Typography,} from '@material-ui/core';
import Service from '../../Service/Service';
import Recaptcha from 'react-recaptcha';
import './Login.css'

class Register extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isVerified:false,
    };
  }
  handlesubmit =(event)=>{
    event.preventDefault();
    const form = document.querySelector('form');
    const data = new FormData(form);
    const jsondata = Object.fromEntries(data.entries());
    if (this.state.isVerified===true) {
      if(jsondata.password === jsondata.Repassword){
      Service.saveData("user",jsondata).then((res)=>{
        if (res.data.status === "STATUS 200 OK") {
          alert(res.data.messages)
          window.location.href='http://localhost:3000/'
        } else {
          alert(res.data.messages)
        }
      })
      }else{alert("Re type not match")}

    } else {
      alert('Captcha not match')
    }
  }

  verifyCallback=(response) =>{
    if (response) {
      this.setState({
        isVerified: true
      })
    }
  }

  recaptchaLoaded=()=> {
    console.log('capcha successfully loaded');
  }
  render(){
  return (
    <Grid container component="main" className='login-container'>
      <Grid item xs={false} sm={4} md={7} className='background-image'/>
      <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
        <div className='paper'>
          <Typography component="h1" variant="h5">
            Register
          </Typography>
          <form className='login-form'>
            <input name="username" type='text' placeholder='UserName'></input>
            <input name="password" type='password' placeholder='Password'></input>
            <input name="Repassword" type='password' placeholder='Re-Type Password'></input>
            <br/><br/>
            <Recaptcha
            sitekey="6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI"
            render="explicit"
            onloadCallback={this.recaptchaLoaded}
            verifyCallback={this.verifyCallback}
          />
            <br></br>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              onClick= {this.handlesubmit}
            >
              Submit
            </Button>
            <Grid container>
              <Grid item xs>
              </Grid>
              <Grid item>
                <Link href="http://localhost:3000/" variant="body2">
                  {"Already have an account? Sign In"}
                </Link>
              </Grid>
            </Grid>
          </form>
          
        </div>
      </Grid>
    </Grid>
  );}
}

export default (Register);