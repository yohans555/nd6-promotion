import React from 'react';
import {Button,Link,Paper,Grid,Typography,} from '@material-ui/core';
import Service from '../../Service/Service';
import Recaptcha from 'react-recaptcha';
import './Login.css'


class SignIn extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isVerified:false,
    };
  }
  handleLogin =(event)=>{
    event.preventDefault();
    const form = document.querySelector('form');
    const data = new FormData(form);
    const jsondata = Object.fromEntries(data.entries());
    if (this.state.isVerified===true) {
    Service.getDataPostMethod("login",jsondata).then((res)=>{
      if (res.data.status === "STATUS 200 OK") {
        localStorage.setItem("user", "Logged In");
        window.location.href='http://localhost:3000/promotion'
      } else {
        alert(res.data.messages)
      }
    })} else {alert('Captcha do not match')}
  }

  verifyCallback=(response) =>{
    if (response) {
      this.setState({
        isVerified: true
      })
    }
  }
  render(){
  return (
    <Grid container component="main" className='login-container'>
      <Grid item xs={false} sm={4} md={7} className='background-image' />
      <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
        <div className='paper'>
          <Typography component="h1" variant="h5">
            Sign in
          </Typography>
          <form className='login-form'>
            <input name="username" type='text' placeholder='UserName'></input>
            <input name="password" type='password' placeholder='Password'></input>
            <br/><br/>
            <Recaptcha
            sitekey="6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI"
            render="explicit"
            onloadCallback={()=>{}}
            verifyCallback={this.verifyCallback}
          />
            <br></br>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              onClick= {this.handleLogin}
            >
              Sign In
            </Button>
            <Grid container>
              <Grid item xs>
                <Link href="#" variant="body2">
                  Forgot password?
                </Link>
              </Grid>
              <Grid item>
                <Link href="http://localhost:3000/register" variant="body2">
                  {"Don't have an account? Sign Up"}
                </Link>
              </Grid>
            </Grid>
          </form>
        </div>
      </Grid>
    </Grid>
  );}
}

export default (SignIn);