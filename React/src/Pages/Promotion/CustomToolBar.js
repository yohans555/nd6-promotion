import React from "react";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import Service from "../../Service/Service";
import ModalEdit from "../../components/ModalPromo/ModalEditPromo";

class CustomToolbarSelect extends React.Component {
  handleClick = () => {
    var yes = window.confirm("are you sure?");
    if(yes){
      Service.deleteData(
        "promotion",
        this.props.items[this.props.selectedRows.data[0].dataIndex].id
      );
    window.location.reload()
    }     
  }
  render() {
    return (
      <div className={"custom-toolbar-select"}>
        <ModalEdit data={ this.props.items[this.props.selectedRows.data[0].dataIndex]}/>
          <IconButton  onClick={this.handleClick}>
            <DeleteIcon className='delete-icon'/>
          </IconButton>    
      </div>
    );
  }

}

export default (CustomToolbarSelect);