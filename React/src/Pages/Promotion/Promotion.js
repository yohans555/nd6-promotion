import React, { Component } from "react";
import Service from "../../Service/Service";
import "./Promotion.css";
import MUIDataTable from "mui-datatables";
import CustomToolbarSelect from './CustomToolBar'
import Details from "../../components/DetailTable/DetailPromodiscount";
import DetailsFG from "../../components/DetailTable/DetailPromoFreegood";
import ModalNew from '../../components/ModalPromo/ModalNewPromo'
import { TablePagination, TableFooter } from "@material-ui/core";

class Promotion extends Component {
  constructor(props) {
    super(props);
    this.state = {
      q:'',
      q2:'',
      rowsPerPage:7,
      page:0,
      items: [],
    };
    this.handleChangePage=this.handleChangePage.bind(this);
  }

  fetchItems(){
    Service.getPage('promotion?page='+this.state.page.toString()+'&elements='+this.state.rowsPerPage.toString()+'&id='+this.state.q+'&pId='+this.state.q2,'').then(
      (res)=>{this.setState({ items: res.data })})
  }
  componentDidMount() {
    this.fetchItems()
  }

  handlechange(e){
    this.setState({q:e.target.value},()=>{
    this.fetchItems()
    })
  }

  handleChangePage = (event, newPage) => {
    this.setState({ page: newPage },()=>{
     this.fetchItems()
    });
  };

  handleChangeRowsPerPage = (event) => {
      this.setState({ rowsPerPage: +event.target.value });
      this.setState({ page: 0 },()=>{
        this.fetchItems()
      });
    };

  handlechange2(e){
    this.setState({q2:e.target.value},()=>{
      this.fetchItems()
    })
  }

  render() {
    const {items} = this.state
    const columns = [
      {
        name: "id",
        label: "DOCUMENT_ID",
        options: {
          filter: false,
          sort: false,
        },
      },
      {
        name: "documentDate",
        label: "DOCUMENT_DATE",
        options: {
          filter: false,
          sort: false,
        },
      },
      {
        name: "promoCategory",
        label: "PROMO_CATEGORY",
        options: {
          filter: true,
          sort: false,
        },
      },
      {
        name: "description",
        label: "DESCRIPTION",
        options: {
          filter: false,
          sort: false,
          customBodyRenderLite: (dataIndex) => {
            try {
              let val = this.state.items[dataIndex].description;
              if (val.length >35) {
                val = val.substring(0,35)+ '....'
              }
              return val;
            } catch (error) {
              return ''
            }
            
          }
        },
      },
    ]

    const options = {
      pagination:false,
      search:false,
      selectableRows:'single',
      selectableRowsOnClick:true,
      customToolbar:()=><ModalNew/> ,
      customToolbarSelect: (selectedRows) => <CustomToolbarSelect items={this.state.items} selectedRows={selectedRows} />,
      expandableRows: true, 
      setRowProps: (row,meta,rowIndex) => { 
        if ((rowIndex%2 === 0)) {
          return {
            style: { background: "wheat" }
          };
        }
      },
      renderExpandableRow: (rowData) => {
        const data = items[items.findIndex((response)=>{return response.id===rowData[0]})]
        if (rowData[2] === "discount") {
          return (
            <>
            <Details item={data}/>
            </>
          )
        } else {
          return (
            <>
            <DetailsFG item={data}/>
            </>
          );
        }
      },
    }
    return (
      <div id="body">
        <div id="contents">
          <br></br>
          <MUIDataTable
            title={
            <div className='search-bar'>
                <h6 className='heading-6'><b>Promotion Rules</b></h6> 
                <input type='text' value={this.state.q} id='search-input' placeholder='Search By Document' onChange={(e)=>this.handlechange(e)}/>
                <input type='text'value={this.state.q2} placeholder='Search By Product ID' onChange={(e)=>this.handlechange2(e)}/>
            </div> }
            data={this.state.items}
            columns={columns}
            options={options}
          />
          <TableFooter>
            <TablePagination
            rowsPerPageOptions={[10,7,5]}
            component="div"
            count={100}
            rowsPerPage={this.state.rowsPerPage}
            page={this.state.page}
            onChangePage={this.handleChangePage}
            onChangeRowsPerPage={this.handleChangeRowsPerPage}
            >
            </TablePagination>
          </TableFooter>
        </div>
      </div>
    );
  }
}
export default Promotion;
