import React from "react";
import ReactToPrint from "react-to-print";
import Invoice from "./Invoice";
import PrintIcon from "@material-ui/icons/Print";
import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";

class PrintInvoice extends React.PureComponent {
  render() {
    return (
      <div style={{ marginTop: "auto", marginBottom: "auto" }}>
        <ReactToPrint
          trigger={() => (
            <Tooltip title={"Print"}>
              <IconButton>
                <PrintIcon />
              </IconButton>
            </Tooltip>
          )}
          content={() => this.componentRef}
        />
        <Invoice
          ref={(el) => (this.componentRef = el)}
          data={this.props.data}
        />
      </div>
    );
  }
}

export default PrintInvoice;
