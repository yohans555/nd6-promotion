import React from 'react';
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TableFooter,
} from "@material-ui/core";
import './Print.css'

class Invoice extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      item:{},
    };
  }
  render(){
    const tProduct = this.props.data.transactionProduct 
    const item = this.props.data
  return (
    <div className='Print-area'>
      <div className='kop-invoice'>
        <div class="col-1">
          <ul>
            <li><h5><b> PT {item.customer.name}</b></h5></li>
            <li><h6>Jl Jendral Sudirman Kav6 no 15</h6></li>
            <li><h6>{item.customer.address}</h6></li>
            <li><h6>Email : Nexsoft_1 @nex.co.id</h6></li>
            <li><h6>Phone : 0897152962</h6></li>
          </ul>
        </div>
        <div class="col-2">
          <ul>
            <li><h6>Jakarta ,{this.props.data.date}</h6></li>
            <li><h6>Kepada Yth:</h6></li>
            <li><h6>PT {item.customer.name}</h6></li>
          </ul>
        </div>
        
      </div>
      <h6><b>FAKTUR NO :{item.id}</b></h6>
      <TableContainer id="table-container">
        <Table id='table-invoice'>
          <TableHead>
            <TableRow>
              <TableCell>Quantity</TableCell>
              <TableCell>Product Name</TableCell>
              <TableCell>Price/pcs</TableCell>
              <TableCell>Discount</TableCell>
              <TableCell>Sum</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {tProduct?(tProduct.map((res)=>(
            <TableRow>
              <TableCell>{res.quantity}</TableCell>
              <TableCell>{res.name}</TableCell>
              <TableCell>{res.price}</TableCell>
              <TableCell>{res.discount} %</TableCell>
              <TableCell>{res.price * res.quantity}</TableCell>
            </TableRow>
            )) ):
            (<p>Nothing In Cart</p>)}
          </TableBody>
          <TableFooter>
            <TableRow>
                <TableCell>Sub Total</TableCell>
                <TableCell>{this.props.data.subTotal}</TableCell>
            </TableRow>
            <TableRow>
                <TableCell>Discount</TableCell>
                <TableCell>{this.props.data.discount}</TableCell>
            </TableRow>
            <TableRow>
                <TableCell>Total</TableCell>
                <TableCell>{this.props.data.total}</TableCell>
            </TableRow>
          </TableFooter>
        </Table>
      </TableContainer>
      <br></br>
      <div class="kop-invoice">
        <div class="col-1">
          <h6>Dengan Hormat</h6><br/>
          <h6>(Sales)</h6>
        </div>
        <div class="col-2">
          <h6>Tanda Terima</h6><br/>
          <h6>(Sales)</h6>
        </div>
      </div>
    </div>
  );}
}

export default Invoice;