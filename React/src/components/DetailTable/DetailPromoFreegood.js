/* eslint-disable no-use-before-define */
import React, { Component } from "react";
import { TableCell,TableRow } from "@material-ui/core";


class DetailsFG extends Component {
  render() {
    const item = this.props.item;
    return (
      <>
        <TableRow>
            <TableCell colSpan={1} align='left' style={{border:'1px solid white'}}></TableCell>
            <TableCell colSpan={1} style={{borderBlockColor:'red'}}>Document Header</TableCell>
        </TableRow>
        <TableRow>
            <TableCell  colSpan={2} style={{border:'1px solid white'}}></TableCell>
            <TableCell colSpan={1} align='left'>Document Number</TableCell>
            <TableCell colSpan={2} align='left'>{item.id}</TableCell>
        </TableRow>
        <TableRow>
            <TableCell colSpan={2} style={{border:'1px solid white'}}></TableCell>
            <TableCell colSpan={1} align='left'>Validity Period</TableCell>
            <TableCell colSpan={2} align='left'>{item.startDate}/{item.endDate}</TableCell>
        </TableRow>
        <TableRow>
            <TableCell colSpan={2} style={{border:'1px solid white'}}></TableCell>
            <TableCell colSpan={1} align='left'>Description</TableCell>
            <TableCell colSpan={2} align='left'>{item.description}</TableCell>
        </TableRow>
        <TableRow>
            <TableCell colSpan={1} style={{border:'1px solid white'}}></TableCell>
            <TableCell colSpan={1} style={{borderBlockColor:'red'}}>What Customer Get</TableCell>
        </TableRow>
        <TableRow>
            <TableCell colSpan={2} style={{border:'1px solid white'}}></TableCell>
            <TableCell colSpan={1} align='left'>Method</TableCell>
            <TableCell colSpan={2} align='left'>Value</TableCell>
        </TableRow>
        <TableRow>
            <TableCell colSpan={2} style={{border:'1px solid white'}}></TableCell>
            <TableCell align='left'>Free Good</TableCell>
            <TableCell align='left'>{item.promoFreeGood.product.productId}--{item.promoFreeGood.product.name}</TableCell>
            <TableCell align='left'>Qty :{item.promoFreeGood.quantity}</TableCell>
        </TableRow>
        <TableRow>
            <TableCell colSpan={2} style={{border:'1px solid white'}}></TableCell>
            <TableCell colSpan={1} align='left'>For Purchase</TableCell>
            <TableCell colSpan={2} align='left'>{item.forPurchase}</TableCell>
        </TableRow>
        <TableRow>
            <TableCell colSpan={1} style={{border:'1px solid white'}}></TableCell>
            <TableCell colSpan={1} style={{borderBlockColor:'red'}}>What Customer Buy</TableCell>
        </TableRow>
        <TableRow>
            <TableCell colSpan={2} align='left' style={{border:'1px solid white'}}></TableCell>
            <TableCell>Id</TableCell>
            <TableCell colSpan={1} align='left'>Name</TableCell>
            <TableCell colSpan={1} align='left'>Price</TableCell>
        </TableRow>
        {item.promoProduct.map((res)=>(
                <TableRow>
                    <TableCell colSpan={2} style={{border:'1px solid white'}}></TableCell>
                    <TableCell>{res.productId}</TableCell>
                    <TableCell colSpan={1} align='left'>{res.name}</TableCell>
                    <TableCell colSpan={1} align='left'>{res.price}</TableCell>
                </TableRow>
        ))}
        <TableRow>
            <TableCell  align='left'></TableCell>
        </TableRow>
       
      </>
    );
  }
}

export default DetailsFG;
