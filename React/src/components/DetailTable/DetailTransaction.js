/* eslint-disable no-use-before-define */
import React, { Component } from "react";
import { TableCell,TableRow } from "@material-ui/core";


class DetailsTransaction extends Component {
  render() {
    const item = this.props;
    return (
      <>
        <TableRow>
            <TableCell colSpan={1} align='left' style={{border:'1px solid white'}}></TableCell>
            <TableCell style={{borderBlockColor:'red'}}>Document Header</TableCell>
        </TableRow>
        <TableRow>
            <TableCell colSpan={2} style={{border:'1px solid white'}}></TableCell>
            <TableCell colSpan={1} align='left'>Document Number</TableCell>
            <TableCell colSpan={2} align='left'>{item.data.id}</TableCell>
        </TableRow>
        <TableRow>
            <TableCell colSpan={2} style={{border:'1px solid white'}}></TableCell>
            <TableCell colSpan={1} align='left'>Document Date</TableCell>
            <TableCell colSpan={2} align='left'>{item.data.date}</TableCell>
        </TableRow>
        <TableRow>
            <TableCell colSpan={2} style={{border:'1px solid white'}}></TableCell>
            <TableCell colSpan={1} align='left'>Document Description</TableCell>
            <TableCell colSpan={2} align='left'>{item.data.customer.name}</TableCell>
        </TableRow>
        <TableRow>
            <TableCell colSpan={1} align='left' style={{border:'1px solid white'}}></TableCell>
            <TableCell style={{borderBlockColor:'red'}}>Transaction Total</TableCell>
        </TableRow>
        <TableRow>
            <TableCell colSpan={2} style={{border:'1px solid white'}}></TableCell>
            <TableCell colSpan={1} align='left'>Total Product</TableCell>
            <TableCell colSpan={2} align='left'>{item.data.product_quantity}</TableCell>
        </TableRow>
        <TableRow>
            <TableCell colSpan={2} style={{border:'1px solid white'}}></TableCell>
            <TableCell colSpan={1} align='left'>Product Variant</TableCell>
            <TableCell colSpan={2} align='left'>{item.data.product_variant}</TableCell>
        </TableRow>
        <TableRow>
            <TableCell colSpan={2} style={{border:'1px solid white'}}></TableCell>
            <TableCell colSpan={1} align='left'>Total Gross Amount</TableCell>
            <TableCell colSpan={2} align='left'>{item.data.subTotal}</TableCell>
        </TableRow>
        <TableRow>
            <TableCell colSpan={2} style={{border:'1px solid white'}}></TableCell>
            <TableCell colSpan={1} align='left'>Discount</TableCell>
            <TableCell colSpan={2} >{item.data.discount}</TableCell>
        </TableRow>
        <TableRow>
            <TableCell colSpan={2} style={{border:'1px solid white'}}></TableCell>
            <TableCell colSpan={1} >Total</TableCell>
            <TableCell colSpan={2} >{item.data.total}</TableCell>
        </TableRow>
        <TableRow>
            <TableCell colSpan={1}  style={{border:'1px solid white'}}></TableCell>
            <TableCell style={{borderBlockColor:'red'}} >Transaction Detail</TableCell>
        </TableRow>
        <TableRow>
            <TableCell align='right'>Id</TableCell>
            <TableCell >Name</TableCell>
            <TableCell >Price</TableCell>
            <TableCell >Quantity</TableCell>
            <TableCell >Sum</TableCell>
        </TableRow>
        {item.data.transactionProduct.map((res)=>(
                <TableRow>
                    <TableCell align='right'>{res.productId}</TableCell>
                    <TableCell >{res.name}</TableCell>
                    <TableCell >{res.price}</TableCell>
                    <TableCell >{res.quantity}</TableCell>
                    <TableCell >{res.quantity * res.price}</TableCell>
                </TableRow>
        ))}
       <TableRow>
            <TableCell colSpan={1} align='left' style={{border:'1px solid white'}}></TableCell>
        </TableRow>
      </>
    );
  }
}

export default DetailsTransaction;
