import React, { Component } from "react";
import Service from "../../Service/Service";
import './NewPromotion.css'
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import PromoValidator from "../../Service/Validator/NewPromoValidator";

class NewPromoDiscount extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data:'',
    };
  }
  send() {
    let jsondata= {
        startDate:document.getElementById('startDate').value,
        endDate:document.getElementById('endDate').value,
        promoProduct: this.state.data,
        forPurchase:document.getElementById('forPurchase').value,
        promoCategory:"discount",
        description:document.getElementById('description').value,
        discount:document.getElementById('discount').value
    };
    if(PromoValidator.discount(jsondata)){
      Service.saveData("promotion", jsondata).then((res)=>{
        if (res.data.status === "STATUS 200 OK") {
          window.location.href = "http://localhost:3000/promotion";
          alert(res.data.status + res.data.messages)
        } else {
          alert(res.data.status+ res.data.messages)
        }
      })
      
      };
    }

  render() {
    return (
      <>
            <Autocomplete
              onChange={(event, value) =>this.setState({data:value}) }
              multiple={true}
              options={this.props.products}
              getOptionLabel={(option) => (option.productId + '-'+option.name +'-'+option.price)}
              style={{ width: 300}}
              renderInput={(params) => <TextField {...params} label="Product List" variant="outlined" size="small"  />}
            />
            <div className='validthru'>
              <input type="date" id="startDate" />
              <span style={{margin:'auto',padding:'15px'}}>Until</span>
              <input type="date" id="endDate" />
            </div>
            <input type="text" id="description" placeholder='Description'/>
            <input type="number" id="forPurchase"  placeholder='Minimum Transaction' />
            <input type="number" id="discount"  placeholder='Discount' />
            <button onClick={this.send.bind(this)}>Submit</button>
        </>
    );
  }
}
export default NewPromoDiscount;
