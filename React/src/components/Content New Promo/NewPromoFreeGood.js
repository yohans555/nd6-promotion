import React, { Component } from "react";
import Service from "../../Service/Service";
import './NewPromotion.css'
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import Promovalidator from '../../Service/Validator/NewPromoValidator'

class NewPromoFreeGood extends Component {
  constructor(props) {
    super(props);
    this.state = {
      freeGood:'',
      promoProduct:'',
    };
  }

  sendts() {
    let jsondata= {
        startDate:document.getElementById('startDate').value,
        endDate:document.getElementById('endDate').value,
        promoProduct: this.state.promoProduct,
        forPurchase:document.getElementById('forPurchase').value,
        promoCategory:"freegood",
        description:document.getElementById('description').value,
        quantity:document.getElementById('quantity').value,
        freeGood :this.state.freeGood,
    };
    if (Promovalidator.freeGood(jsondata)) {
      Service.saveData("promotion", jsondata).then((res)=>{
        if (res.data.status === "STATUS OK 200") {
          alert(res.data.status + res.data.messages)
          window.location.reload()
        } else {
          alert(res.data.status+ res.data.messages)
        }
      })
    }
  }

  render() {
    return (
      <>
            <Autocomplete
              onChange={(event, value) =>this.setState({promoProduct:value}) }
              multiple={true}
              name='promoProduct'
              style={{ width: 300}}
              options={this.props.products}
              getOptionLabel={(option) => (option.productId + '-'+option.name +'-'+option.price)}
              renderInput={(params) => <TextField {...params} label="Product List" variant="outlined" size="small"  />}
            />
            <div className='validthru'>
              <input type="date" id="startDate" />
              <span style={{margin:'auto',padding:'15px'}}>Until</span>
              <input type="date" id="endDate" />
            </div>
            <input required id="description" placeholder="Description" />
            <input type="number" id="forPurchase" placeholder='minimum transaction' min="1" />
            <h6>Pilih Free Good yang akan didapat</h6>
            <Autocomplete
              id="select-option"
              onChange={(event, value) =>this.setState({freeGood:value}) }
              options={this.props.products}
              getOptionLabel={(option) => (option.productId + '-'+option.name+'-'+option.price)}
              style={{ width: 300}}
              renderInput={(params) => <TextField {...params} label="Pilih Free Good" variant="outlined"  />}
            />
            <input type="number" id="quantity" placeholder="qty" />
            
            <button onClick={this.sendts.bind(this)}>Submit</button>
     </>
    );
  }
}
export default NewPromoFreeGood
