import React from 'react';
import {AppBar,Tabs,Tab,Typography,Box,} from '@material-ui/core';
import PersonPinIcon from '@material-ui/icons/PersonPin';
import HelpIcon from '@material-ui/icons/Help';
import ShoppingBasket from '@material-ui/icons/ShoppingBasket';
import ThumbDown from '@material-ui/icons/ThumbDown';
import ThumbUp from '@material-ui/icons/ThumbUp';
import NewPromoDiscount from './NewPromotionDiscount';
import NewPromoFreeGood from './NewPromoFreeGood';
import LoyaltyIcon from '@material-ui/icons/Loyalty';
import LocalMallIcon from '@material-ui/icons/LocalMall';
import { connect } from 'react-redux'

function TabPanel(props) {
  const { children, value, index, ...other } = props;
  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`scrollable-force-tabpanel-${index}`}
      aria-labelledby={`scrollable-force-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

class NewPromoPanel extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        value: 0,
    };
  }
  handlechange=(e)=>{
    this.setState({value:e})
  }

  render(){
  const {value} =this.state;
  return (
    <div style={{backgroundColor:'white'}}>
      <AppBar position="static" color="default">
        <Tabs
          value={value}
          onChange={(e,r)=>this.handlechange(r)}
          variant="scrollable"
          scrollButtons="on"
          indicatorColor="primary"
          textColor="primary"
          aria-label="scrollable force tabs example"
        >
          <Tab label="Discount" icon={<LoyaltyIcon />}/>
          <Tab label="Free Good" icon={<LocalMallIcon />} />
          <Tab label="Coupon" icon={<PersonPinIcon />} />
          <Tab label="Member" icon={<HelpIcon />}/>
          <Tab label="Delivery" icon={<ShoppingBasket />}/>
          <Tab label="credit card" icon={<ThumbDown />}/>
          <Tab label="others" icon={<ThumbUp />}/>
        </Tabs>
      </AppBar>
      <TabPanel value={value} index={0}>
        <NewPromoDiscount products={this.props.products}/>
      </TabPanel>
      <TabPanel value={value} index={1}>
        <NewPromoFreeGood products={this.props.products}/>
      </TabPanel>
      <TabPanel value={value} index={2}>
        Item Three
      </TabPanel>
      <TabPanel value={value} index={3}>
        Item Four
      </TabPanel>
      <TabPanel value={value} index={4}>
        Item Five
      </TabPanel>
      <TabPanel value={value} index={5}>
        Item Six
      </TabPanel>
      <TabPanel value={value} index={6}>
        Item Seven
      </TabPanel>
    </div>
  );
}}

const mapStateToProps = (state)=>{
  return{
      products: state.items,
  }
}
export default connect(mapStateToProps)(NewPromoPanel)