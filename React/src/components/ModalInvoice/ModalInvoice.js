import React from "react";
import { Button, Modal, Tooltip, Backdrop, Fade } from "@material-ui/core";
import ReceiptIcon from "@material-ui/icons/Receipt";
import Service from "../../Service/Service";
import Invoice from "./Invoice";
import { connect } from "react-redux";
import SaveIcon from "@material-ui/icons/Save";
import CloseIcon from "@material-ui/icons/Close";
import TransactionValidator from "../../Service/Validator/NewTransactionValidator";

class ModalInvoice extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      data: {},
    };
  }
  handleOpen = () => {
    let jsondata = {
      cart: this.props.cart,
      customer: this.props.customer,
    };
    Service.getDataPostMethod2("invoice", jsondata).then((res) =>
      this.setState({ data: res.data }, () => {
        this.setState({ open: true });
      })
    );
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  //to submit cart
  handleSave = () => {
    let target = "transaction";
    if (this.props.type === "edit") {
      target = "edit-transaction/" + localStorage.getItem("id");
    }
    let jsondata = {
      cart: this.props.cart,
      customer: this.props.customer,
    };
    if (TransactionValidator.validate(jsondata)) {
      Service.saveData(target, jsondata).then((res) => {
        if (res.data.status === "STATUS 200 OK") {
          window.location.href = "http://localhost:3000/transaction";
          alert(res.data.status + res.data.messages);
        } else {
          alert(res.data.status + res.data.messages);
        }
      });
    }
  };
  render() {
    return (
      <div>
        <Tooltip title={"Checkout"}>
          <Button
            onClick={this.handleOpen}
            variant="contained"
            color="primary"
            startIcon={<ReceiptIcon />}
            style={{marginRight:'15px'}}
          >
            {this.props.type === "edit" ? "submit" : "CheckOut"}
          </Button>
        </Tooltip>
        <Modal
          aria-labelledby="transition-modal-title"
          aria-describedby="transition-modal-description"
          id="modal-global"
          open={this.state.open}
          onClose={this.handleClose}
          closeAfterTransition
          BackdropComponent={Backdrop}
          BackdropProps={{
            timeout: 500,
          }}
        >
          <Fade in={this.state.open}>
            <div className="paper-global" style={{ overflowY: "auto" }}>
              <div id="header-modal">
                <Button startIcon={<CloseIcon />} onClick={this.handleClose} color="secondary">
                  Cancel
                </Button>
                <Button
                  onClick={this.handleSave}
                  startIcon={<SaveIcon />}
                  color="primary"
                >
                  Save
                </Button>
              </div>
              <Invoice type={this.props.type} data={this.state.data} />
            </div>
          </Fade>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    data: state.tempData,
    cart: state.addedItems,
    customer: state.selectedCustomer,
  };
};

export default connect(mapStateToProps)(ModalInvoice);
