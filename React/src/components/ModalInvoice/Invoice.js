import React from 'react';
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TableFooter
} from "@material-ui/core";
class Invoice extends React.Component {
     
  render(){
    const tProduct = this.props.data.transactionProduct 
  return (
    <div >
      <TableContainer>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Quantity</TableCell>
              <TableCell>Product Name</TableCell>
              <TableCell>Price/pcs</TableCell>
              <TableCell>Discount</TableCell>
              <TableCell>Sum</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {tProduct?(tProduct.map((res)=>(
            <TableRow>
              <TableCell>{res.quantity}</TableCell>
              <TableCell>{res.name}</TableCell>
              <TableCell>{res.price}</TableCell>
              <TableCell>{res.discount} %</TableCell>
              <TableCell>{res.price * res.quantity}</TableCell>
            </TableRow>
            )) ):
            (<p>Nothing In Cart</p>)}
          </TableBody>
          <TableFooter>
            <TableRow>
                <TableCell>Sub Total</TableCell>
                <TableCell>{this.props.data.subTotal}</TableCell>
            </TableRow>
            <TableRow>
                <TableCell>Discount</TableCell>
                <TableCell>{this.props.data.discount}</TableCell>
            </TableRow>
            <TableRow>
                <TableCell>Total</TableCell>
                <TableCell>{this.props.data.total}</TableCell>
            </TableRow>
          </TableFooter>
        </Table>
      </TableContainer>
    </div>
  );}
}

export default Invoice;