
import  { Component } from 'react';
import React from 'react';
import * as FaIcons from 'react-icons/fa';
import * as AiIcons from 'react-icons/ai';
import { Link } from 'react-router-dom';
import { SidebarData } from './SidebarData';
import './Navbar.css';
import { IconContext } from 'react-icons';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';

class Navbar extends Component{
  constructor(props) {
    super(props);
    this.state = {
      sidebar: false
    };
  }

  showSidebar = ()=>{
    this.setState({sidebar:!this.state.sidebar})
  }

  handleLogout =()=>{
    localStorage.removeItem("user");
  }

  render(){
    return(
      <>
      <IconContext.Provider value={{ color: '#fff' }}>
      <div className='navbar'>
        <Link to='#' className='menu-bars'>
          <FaIcons.FaBars onClick={this.showSidebar} />
        </Link>
          <a href='/' id={localStorage.getItem('user')!=null?('logout-button'):('hide-button')}  onClick={this.handleLogout}>
            <ExitToAppIcon/> Sign Out
          </a>
      </div>
      <nav className={this.state.sidebar ? 'nav-menu active' : 'nav-menu'}>
        <ul className='nav-menu-items' onClick={this.showSidebar}>
          <li className='navbar-toggle'>
            <Link to='#' className='menu-bars'>
              <AiIcons.AiOutlineClose />
            </Link>
          </li>
          {SidebarData.map((item, index) => {
            return (
              <li key={index} className={item.cName}>
                <Link to={item.path}>
                  {item.icon}
                  <span>{item.title}</span>
                </Link>
              </li>
            );
          })}
        </ul>
      </nav>
    </IconContext.Provider>
  </>
)
  }
}
  export default Navbar