import React, { Component } from 'react';
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { removeItem,addQuantity,subtractQuantity} from '../../Redux/actions/cartActions'
import {
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    TablePagination,
    TableFooter,IconButton 
  } from "@material-ui/core";
import ModalInvoice from '../ModalInvoice/ModalInvoice';
import ModalPromo from '../ModalGeneratePromo/ModalPromo';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';

class Cart extends Component{
    constructor(props) {
        super(props);
        this.state = {
            page: 0,
            rowsPerPage: 5,
        };
      }
    //to remove the item completely
    handleRemove = (id)=>{
        this.props.removeItem(id);
    }
    //to add the quantity
    handleAddQuantity = (id)=>{
        this.props.addQuantity(id);
    }
    //to substruct from the quantity
    handleSubtractQuantity = (id)=>{
        this.props.subtractQuantity(id);
    }

    handleChangePage = (event, newPage) => {
        this.setState({ page: newPage });
      };
    
    handleChangeRowsPerPage = (event) => {
        this.setState({ rowsPerPage: +event.target.value });
        this.setState({ page: 0 });
      };

    render(){
       return(
                <div className="cart">
                    <TableContainer>
                           <Table>
                               <TableHead>
                                    <TableRow>
                                        <TableCell>Id</TableCell>
                                        <TableCell>Name</TableCell>
                                        <TableCell>Price</TableCell>
                                        <TableCell>Quantity</TableCell>
                                    </TableRow>
                               </TableHead>
                               <TableBody className='table-body'>
                               {this.props.items.slice(
                                this.state.page * this.state.rowsPerPage,
                                this.state.page * this.state.rowsPerPage +
                                this.state.rowsPerPage
                                ).map(item=>(
                                <TableRow>
                                    <TableCell className='table-body'>{item.productId}</TableCell>
                                    <TableCell>{item.name}</TableCell>
                                    <TableCell>{item.price}</TableCell>
                                    <TableCell >
                                        <div style={{display:'inline-flex'}}>
                                            <Link to={this.props.type ==='edit'?("/edit"):("/new-transaction")}><i className="material-icons" onClick={()=>{this.handleSubtractQuantity(item.productId)}}>arrow_drop_down</i></Link>
                                            {item.quantity}
                                            <Link to={this.props.type ==='edit'?("/edit"):("/new-transaction")}><i className="material-icons" onClick={()=>{this.handleAddQuantity(item.productId)}}>arrow_drop_up</i></Link>
                                            <IconButton size='small' onClick={()=>{this.handleRemove(item.productId)}}>
                                            <HighlightOffIcon />
                                            </IconButton>
                                        </div>
                                    </TableCell>
                                </TableRow>
                                ))
                            }     
                               </TableBody>
                           </Table>
                           <TableFooter>
                            <TablePagination
                                rowsPerPageOptions={[10,5]}
                                component="div"
                                count={this.props.items.length}
                                rowsPerPage={this.state.rowsPerPage}
                                page={this.state.page}
                                onChangePage={this.handleChangePage}
                                onChangeRowsPerPage={this.handleChangeRowsPerPage}
                            />
                            </TableFooter>
                       </TableContainer>
                       <br></br>
                       <div className="action">
                            <h6 className="collection-item"><b>Total: {this.props.total}</b></h6>
                            <ModalInvoice type={this.props.type}/>
                            <ModalPromo data={this.props.items}/>
                       </div>
                </div> 
       )
    }
}

const mapStateToProps = (state)=>{
    return{
        items: state.addedItems,
        total: state.total,
    }
}
const mapDispatchToProps = (dispatch)=>{
    return{
        removeItem: (id)=>{dispatch(removeItem(id))},
        addQuantity: (id)=>{dispatch(addQuantity(id))},
        subtractQuantity: (id)=>{dispatch(subtractQuantity(id))}
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(Cart)