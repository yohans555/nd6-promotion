import React from "react";
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import NewPromoPanel from "../Content New Promo/NewPromoPanel";
import {Fade,Tooltip,Backdrop,Modal,Button} from "@material-ui/core";

class ModalNew extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
    };
  }

  handleOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };
  render() {
    return (
      <>
        <Tooltip title={"New Promo"}>
          <Button
          onClick={this.handleOpen}
          variant="contained"
          color="primary"
          startIcon={<AddCircleOutlineIcon />}
          >  Add Promo
          </Button>
        </Tooltip>
        <Modal
          id='modal-global'
          open={this.state.open}
          onClose={this.handleClose}
          closeAfterTransition
          BackdropComponent={Backdrop}
          BackdropProps={{
            timeout: 500,
          }}
        >
          <Fade in={this.state.open}>
              <NewPromoPanel/>
          </Fade>
        </Modal>
      </>
    );
  }
}

export default (ModalNew);