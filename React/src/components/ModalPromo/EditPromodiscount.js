/* eslint-disable no-use-before-define */
import React, { Component } from "react";
import { TableCell, TableRow, Button,Table } from "@material-ui/core";
import Service from "../../Service/Service";
import SaveIcon from "@material-ui/icons/Save";
import CloseIcon from "@material-ui/icons/Close";
import { connect } from "react-redux";
import NewPromoValidator from "../../Service/Validator/NewPromoValidator";
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';

class Details extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      item: this.props.item,
    };
  }

  handleChangeDescription = (e) => {
    var temp = { ...this.state.item };
    temp.description = e.target.value;
    this.setState({ item: temp });
  };

  handleChangeForPurchase = (e) => {
    var temp = { ...this.state.item };
    temp.forPurchase = e.target.value;
    this.setState({ item: temp });
  };

  handlePP = (value) => {
    var temp = { ...this.state.item };
    temp.promoProduct = value;
    this.setState({ item: temp });
  };

  handleChangeDiscount = (e) => {
    var temp = { ...this.state.item };
    temp.promoDiscount.discount = e.target.value;
    this.setState({ item: temp });
  };

  handleChangeDate = (e) => {
    var temp = { ...this.state.item };
    temp.endDate = e.target.value;
    this.setState({ item: temp });
  };

  handleSubmit = () => {
    var temp = { ...this.state.item };
    if(NewPromoValidator.discount(temp)){
      Service.saveData("edit-promotion", temp).then((res) => {
        if (res.data.status === "STATUS 200 OK") {
          window.location.href = "http://localhost:3000/promotion";
          alert(res.data.status + res.data.messages);
        } else {
          alert(res.data.status + res.data.messages);
        }
    });
    }
  };

  render() {
    const item = this.state.item;
    return (
      <>
        <div id="header-modal">
          <Button startIcon={<CloseIcon />} color="secondary">
            Cancel
          </Button>
          <Button
            onClick={this.handleSubmit}
            startIcon={<SaveIcon />}
            color="primary"
          >
            Save
          </Button>
        </div>
        <TableRow>
          <TableCell align="center" style={{ borderBlockColor: "red" }}>
            <b>Document Header</b>
          </TableCell>
        </TableRow>
        <TableRow>
          <TableCell align="right" style={{ borderBlockColor: "white" }}>
            <ul>
              <li>
                <label>
                  Document Number :
                  <input value={item.id} type="text" disabled="true" />
                </label>
              </li>
              <li>
                <label>
                  Validity Period :
                  <div className="validthru">
                    <input value={item.startDate} type="date" disabled="true" />
                    <span style={{ margin: "auto", padding: "15px" }}>
                      Until
                    </span>
                    <input
                      type="date"
                      id="endDate"
                      onChange={this.handleChangeDate}
                      value={item.endDate}
                    />
                  </div>
                </label>
              </li>
              <li>
                <label>
                  Description :{" "}
                  <input
                    value={item.description}
                    type="text"
                    onChange={(e) => this.handleChangeDescription(e)}
                  />
                </label>
              </li>
            </ul>
          </TableCell>
        </TableRow>
        <TableRow>
          <TableCell align="center" style={{ borderBlockColor: "red" }}>
            <b>What Customer Get</b>
          </TableCell>
        </TableRow>
        <TableRow>
          <TableCell style={{ borderBlockColor: "white" }}>
            <ul>
              <li>
                <label>
                  Method :
                  <input value={"Value"} type="text" disabled="true" />
                </label>
              </li>
              <li>
                <label>
                  Line Discount :
                  <input
                    value={item.promoDiscount.discount}
                    type="number"
                    onChange={(e) => this.handleChangeDiscount(e)}
                  />
                </label>
              </li>
              <li>
                <label>
                  For Purchase :
                  <input
                    value={item.forPurchase}
                    type="number"
                    onChange={(e) => this.handleChangeForPurchase(e)}
                  />
                </label>
              </li>
            </ul>
          </TableCell>
        </TableRow>
        <TableRow>
          <TableCell align="center" style={{margin:'auto',  borderBlockColor: "red" }} >
            <b>What Customer Buy</b>
          </TableCell>
        </TableRow>
        <TableRow>
          <TableCell>
          <Autocomplete
                  id="select-option"
                  multiple={true}
                  onChange={(event, value) =>this.handlePP(value) }
                  defaultValue={item.promoProduct}
                  options={this.props.products}
                  getOptionLabel={(option) => (option.productId + '-'+option.name+'-'+option.price)}
                  style={{ width: 300}}
                  renderInput={(params) => <TextField {...params} label="Promo Product" variant="outlined"  />}
                />
          </TableCell>
          </TableRow>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    products: state.items,
  };
};
export default connect(mapStateToProps)(Details);
