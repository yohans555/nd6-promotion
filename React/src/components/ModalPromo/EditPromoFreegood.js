/* eslint-disable no-use-before-define */
import React, { Component } from "react";
import { TableCell, TableRow, Button,Table } from "@material-ui/core";
import Service from "../../Service/Service";
import SaveIcon from "@material-ui/icons/Save";
import CloseIcon from "@material-ui/icons/Close";
import { connect } from "react-redux";
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';

class DetailsFG extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      item: this.props.item,
    };
  }

  handleChangeDescription = (e) => {
    var temp = { ...this.state.item };
    temp.description = e.target.value;
    this.setState({ item: temp });
  };

  handleChangeForPurchase = (e) => {
    var temp = { ...this.state.item };
    temp.forPurchase = e.target.value;
    this.setState({ item: temp });
  };

  handleChangeQuantity = (e) => {
    var temp = { ...this.state.item };
    temp.promoFreeGood.quantity = e.target.value;
    this.setState({ item: temp });
  };

  handleChangeDate = (e) => {
    var temp = { ...this.state.item };
    temp.endDate = e.target.value;
    this.setState({ item: temp });
  };
  handleChangeFG = (e) => {
    var temp = { ...this.state.item };
    temp.promoFreeGood.product = e;
    this.setState({ item: temp });
  };

  handleSubmit = () => {
    var temp = { ...this.state.item };
      Service.saveData("edit-promotion", temp).then((res) => {
        if (res.data.status === "STATUS 200 OK") {
          window.location.href = "http://localhost:3000/promotion";
          alert(res.data.status + res.data.messages);
        } else {
          alert(res.data.status + res.data.messages);
        }
    });

  };

  handlePP = (value) => {
    var temp = { ...this.state.item };
    temp.promoProduct = value;
    this.setState({ item: temp });
  };

  render() {
    const item = this.state.item;
    return (
      <>
        <div id="header-modal">
          <Button startIcon={<CloseIcon />} color="secondary">
            Cancel
          </Button>
          <Button
            onClick={this.handleSubmit}
            startIcon={<SaveIcon />}
            color="primary"
          >
            Save
          </Button>
        </div>
        <TableRow>
          <TableCell align="center" style={{ borderBlockColor: "red" }}>
            <b>Document Header</b>
          </TableCell>
        </TableRow>
        <TableRow>
          <TableCell align="right" style={{ borderBlockColor: "white" }}>
            <ul>
              <li>
                <label>
                  Document Number :
                  <input value={item.id} type="text" disabled="true" />
                </label>
              </li>
              <li>
                <label>
                  Validity Period :
                  <div className="validthru">
                    <input value={item.startDate} type="date" disabled="true" />
                    <span style={{ margin: "auto", padding: "15px" }}>
                      Until
                    </span>
                    <input
                      type="date"
                      id="endDate"
                      onChange={this.handleChangeDate}
                      value={item.endDate}
                    />
                  </div>
                </label>
              </li>
              <li>
                <label>
                  Description :{" "}
                  <input
                    value={item.description}
                    type="text"
                    onChange={(e) => this.handleChangeDescription(e)}
                  />
                </label>
              </li>
            </ul>
          </TableCell>
        </TableRow>
        <TableRow>
          <TableCell align="center" style={{ borderBlockColor: "red" }}>
            <b>What Customer Get</b>
          </TableCell>
        </TableRow>
        <TableRow>
          <TableCell style={{ borderBlockColor: "white" }}>
            <ul>
              <li>
                <label>
                  Method :
                  <input value={"Value"} type="text" disabled="true" />
                </label>
              </li>
              <li>
                <Autocomplete
                  id="select-option"
                  onChange={(event, value) =>this.handleChangeFG(value) }
                  defaultValue={item.promoFreeGood.product}
                  options={this.props.products}
                  getOptionLabel={(option) => (option.productId + '-'+option.name+'-'+option.price)}
                  style={{ width: 300}}
                  renderInput={(params) => <TextField {...params} label="Pilih Free Good" variant="outlined"  />}
                /><br/>
                 <label>
                  quantity :
                  <input
                    value={item.promoFreeGood.quantity}
                    type="number"
                    onChange={(e) => this.handleChangeQuantity(e)}
                  />
                </label><br/>
              </li>
              <li>
                <label>
                  For Purchase :
                  <input
                    value={item.forPurchase}
                    type="number"
                    onChange={(e) => this.handleChangeForPurchase(e)}
                  />
                </label>
              </li>
            </ul>
          </TableCell>
        </TableRow>
        <TableRow>
          <TableCell align="center" style={{margin:'auto',  borderBlockColor: "red" }} >
            <b>What Customer Buy</b>
          </TableCell>
        </TableRow>
        <TableRow>
          <TableCell>
          <Autocomplete
                  id="select-option"
                  multiple={true}
                  onChange={(event, value) =>this.handlePP(value) }
                  defaultValue={item.promoProduct}
                  options={this.props.products}
                  getOptionLabel={(option) => (option.productId + '-'+option.name+'-'+option.price)}
                  style={{ width: 300}}
                  renderInput={(params) => <TextField {...params} label="Promo Product" variant="outlined"  />}
                />
          </TableCell>
          </TableRow>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    products: state.items,
  };
};
export default connect(mapStateToProps)(DetailsFG);
