import React from "react";
import {Fade,Tooltip,Backdrop,Modal,Table,IconButton} from "@material-ui/core";
import EditIcon from "@material-ui/icons/Edit";
import Details from "./EditPromodiscount";
import DetailsFG from "./EditPromoFreegood";

class ModalEdit extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false
    };
  }
  handleOpen = () => {
    this.setState({ open: true });
  };

  viewPromo = () => {
    let item = this.props.data;
    switch (this.props.data.promoCategory) {
      case "discount":
        return (
        <Table id="promo-edit">
          <Details item= {item}/>
        </Table>); 
      case "freegood":
        return (
          <Table  id="promo-edit">
            <DetailsFG item= {item}/>
          </Table>); 
      default:
        return <p>Deleted</p>;
    }
  };

  handleClose = () => {
    this.setState({ open: false });
  };
  render() {  
    return (
      <>
        <Tooltip title={"Edit"}>
          <IconButton onClick={this.handleOpen}>
            <EditIcon  className='edit-icon' />
          </IconButton>
        </Tooltip>
        <Modal
          aria-labelledby="transition-modal-title"
          aria-describedby="transition-modal-description"
          id='modal-global'
          open={this.state.open}
          onClose={this.handleClose}
          closeAfterTransition
          BackdropComponent={Backdrop}
          BackdropProps={{
            timeout: 500,
          }}
        >
          <Fade in={this.state.open}>
           <div className='paper-global'>{this.viewPromo()}</div>
          </Fade>
        </Modal>
      </>
    );
  }
}

export default (ModalEdit);
