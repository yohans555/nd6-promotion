import React from "react";
import Service from "../../Service/Service";
import Details from "../DetailTable/DetailPromodiscount";
import DetailsFG from "../DetailTable/DetailPromoFreegood";
import { Button,Fade,Tooltip,Backdrop,Modal,Table} from "@material-ui/core";

class ModalPromo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      dataFinalPromo: {
        promoCategory: "",
        promoDiscount: {
          discount: "",
        },
        promoFreeGood: {
          quantity: "",
        },
      },
    };
  }

  handleOpen = () => {
    let jsondata = {cart: this.props.data};
    Service.getDataPostMethod2("finalpromo", jsondata).then((res) =>
      this.setState({ dataFinalPromo: res.data })
    );
    this.setState({ open: true });
  };

  viewPromo = () => {
    let cases = this.state.dataFinalPromo.promoCategory;
    let item = this.state.dataFinalPromo;
    switch (cases) {
      case "discount":
        return (
        <Table>
          <Details item= {item}/>
        </Table>); 
      case "freegood":
        return (
          <Table>
            <DetailsFG item= {item}/>
          </Table>); 
      default:
        return <p>No Promo For You</p>;
    }
  };

  handleClose = () => {
    this.setState({ open: false });
  };
  render() {
    return (
      <div>
        <Tooltip title={"Edit"}>
          <Button variant="outlined" color="primary" onClick={this.handleOpen}>
            See Promos
          </Button>
        </Tooltip>
        <Modal
          id="modal-global"
          aria-labelledby="transition-modal-title"
          aria-describedby="transition-modal-description"
          open={this.state.open}
          onClose={this.handleClose}
          closeAfterTransition
          BackdropComponent={Backdrop}
          BackdropProps={{
            timeout: 500,
          }}
        >
          <Fade in={this.state.open}>
            <div className='paper-global'>{this.viewPromo()}</div>
          </Fade>
        </Modal>
      </div>
    );
  }
}

export default(ModalPromo);
