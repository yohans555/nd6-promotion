package com.nexsoft.app;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import com.nexsoft.app.DTO.*;
import com.nexsoft.app.model.Customer;
import com.nexsoft.app.model.Promo.Promotion;
import com.nexsoft.app.model.Transaction.Transaction;
import com.nexsoft.app.model.Transaction.TransactionProduct;
import com.nexsoft.app.service.ModelService;
import com.nexsoft.app.service.TransactionCalculator;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

@SpringBootTest
class AppApplicationTests {
	@Autowired
	ModelService modelService;

	@Autowired
	TransactionCalculator transactionCalculator;

	Customer customer = new Customer(1, "test_name", "test_address");

	@Test
	void GenerateCorrectPromoTest_1() {
		List<TransactionProduct> cart = new ArrayList<>();{
			cart.add(new TransactionProduct(1, 1, "product_1", 1, 1000,0));
			cart.add(new TransactionProduct(2, 2, "product_2", 9, 2000,0));
		}
		Promotion result = modelService.getFinalPromo(cart);
		assertTrue(result.getId().equals("PROMO_06"));
	}

	@Test
	void GenerateCorrectPromoTest_2() {
		List<TransactionProduct> cart = new ArrayList<>();{
			cart.add(new TransactionProduct(1, 6, "product_6", 1, 6000,0));
		}		
		Promotion result = modelService.getFinalPromo(cart);
		assertFalse(result.getPromoDiscount()!=null && result.getPromoFreeGood()!=null);
	}

	@Test
	void GenerateCorrectPromoTest_3() {
		List<TransactionProduct> cart = new ArrayList<>();{
			cart.add(new TransactionProduct(1, 1, "product_1", 3, 1000,0));
			cart.add(new TransactionProduct(2, 2, "product_2", 4, 2000,0));
		}		
		Promotion result = modelService.getFinalPromo(cart);
		assertEquals(result.getPromoDiscount().getDiscount(),10);
	}

	@Test
	void GenerateCorrectPromoTest_4() {
		List<TransactionProduct> cart = new ArrayList<>();{
			cart.add(new TransactionProduct(1, 3, "product_3", 4, 3000,0));
			cart.add(new TransactionProduct(2, 4, "product_4", 5, 4000,0));
		}
		Promotion result = modelService.getFinalPromo(cart);
		assertTrue(result.getId().equals("PROMO_04"));
	}

	// MAKE SURE METHOD GENERATE CORRECT TOTAL 
	@Test
	void GenerateCorrectInvoice_1() {
		List<TransactionProduct> cart = new ArrayList<>();{
			cart.add(new TransactionProduct(1, 1, "product_1", 3, 1000,0));
			cart.add(new TransactionProduct(2, 2, "product_2", 4, 2000,0));
		}
		NewTransactionDTO transaction = new NewTransactionDTO(cart, customer);		
		Transaction actual = modelService.getInvoice(transaction);
		Long expectation = (long) 10700;
		assertEquals(actual.getTotal(),expectation);
	}
	// TEST GENERATE INVOICE IF DISCOUNT IN RUPIAH
	@Test
	void GenerateCorrectInvoice_2() {
		List<TransactionProduct> cart = new ArrayList<>();{
			cart.add(new TransactionProduct(1, 7, "product_7", 1, 7000,0));
		}
		NewTransactionDTO transaction = new NewTransactionDTO(cart, customer);		
		Transaction actual = modelService.getInvoice(transaction);
		Long expectation = (long) 6500;
		assertEquals(actual.getTotal(),expectation);
	}

	// TEST GENERATE INVOICE IF PROMO FREEGOOD
	@Test
	void GenerateCorrectInvoice_3() {
		List<TransactionProduct> cart = new ArrayList<>();{
			cart.add(new TransactionProduct(1, 1, "product_1", 2, 1000,0));
			cart.add(new TransactionProduct(1, 3, "product_3", 28, 3000,0));
		}
		NewTransactionDTO transaction = new NewTransactionDTO(cart, customer);		
		Transaction actual = modelService.getInvoice(transaction);
		int expectation = 3;
		assertEquals(actual.getTransactionProduct().size(),expectation);
	}

	// Checking corect amount of discount
	@Test
	void GenerateCorrectInvoice_4() {
		List<TransactionProduct> cart = new ArrayList<>();{
			cart.add(new TransactionProduct(1, 4, "product_4", 2, 4000,0));
			cart.add(new TransactionProduct(1, 3, "product_3", 2, 3000,0));
		}
		NewTransactionDTO transaction = new NewTransactionDTO(cart, customer);		
		Transaction actual = modelService.getInvoice(transaction);
		long expectation = 3200;
		assertEquals(actual.getDiscount(),expectation);
	}


	// Checking for hierarchy of promotion the best promo
	@Test
	void Hieararchy_Test() {
		List<TransactionProduct> cart = new ArrayList<>();{
			cart.add(new TransactionProduct(1, 5, "product_5", 100, 5000,0));
		}
		String final_promo =modelService.getFinalPromo(cart).getId();
		List<Promotion> satisfied_promo = transactionCalculator.promoFinderPossible(cart);
		if (satisfied_promo.size()>1&&final_promo.equalsIgnoreCase("PROMO_03")){
			assertTrue(true);;
		}else
		assertTrue(false);
	}

	// Checking for hierarchy of promotion THE BEST DISCOUNT
	@Test
	void Hieararchy_Test2() {
		List<TransactionProduct> cart = new ArrayList<>();{
			cart.add(new TransactionProduct(1, 6, "product_5", 100, 6000,0));
		}
		String final_promo =modelService.getFinalPromo(cart).getId();
		List<Promotion> satisfied_promo = transactionCalculator.promoFinderPossible(cart);
		if (satisfied_promo.size()>1&&final_promo.equalsIgnoreCase("PROMO_07")){
			assertTrue(true);;
		}else
		assertTrue(false);
	}

	// Checking for hierarchy of promotion BASED ON START DATE IF THE VALUE IS EQUAL
	@Test
	void Hieararchy_Test3() {
		List<TransactionProduct> cart = new ArrayList<>();{
			cart.add(new TransactionProduct(1, 1, "product_1", 3, 1000,0));
		}
		String final_promo =modelService.getFinalPromo(cart).getId();
		List<Promotion> satisfied_promo = transactionCalculator.promoFinderPossible(cart);
		if (satisfied_promo.size()>1&&final_promo.equalsIgnoreCase("PROMO_01")){
			assertTrue(true);;
		}else
		assertTrue(false);
	}

	// MAKE SURE PAGINATION WORKS PROPERLY
	@Test
	void Paging_Test_Promotion() {
		Pageable pageable = PageRequest.of(1, 5);
		List<Promotion> actual = modelService.getAllPromotion("", "", pageable);
		assertEquals(5, actual.size());
	}

	// MAKE SURE PAGINATION WORKS PROPERLY
	@Test
	void Paging_Test_Transaction() {
		Pageable pageable = PageRequest.of(0, 5);
		List<Transaction> actual = modelService.getAllTransaction("", pageable);
		assertEquals(0, actual.size());
	}
}
