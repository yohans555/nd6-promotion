package com.nexsoft.app.repo;

import com.nexsoft.app.model.*;

import org.springframework.data.jpa.repository.JpaRepository;


public interface UserRepository extends JpaRepository<User, Integer> {
    User findByUsernameAndPassword(String username,String password);
    User findByUsername(String username);
}


