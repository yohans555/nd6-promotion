package com.nexsoft.app.repo;

import com.nexsoft.app.model.Promo.PromoDiscount;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PromoDiscountRepository extends JpaRepository<PromoDiscount, Integer> {
}