package com.nexsoft.app.repo;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import com.nexsoft.app.Variable.OtherVariable;
import com.nexsoft.app.model.Promo.Promotion;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


public interface PromotionRepository extends JpaRepository<Promotion, String> {
    @Modifying(clearAutomatically = true)
	@Query(nativeQuery = true, value = "update promotion p set deleted=true where p.id= :id")
	void delete(@Param("id") String id);

    @Query(nativeQuery = true, value = "select * from promotion p where p.deleted=false")
	ArrayList<Promotion> find();

	@Query(nativeQuery = true, value = OtherVariable.QUERY_1)
	List<Promotion> findAffiliatePromotions(@Param("products")ArrayList<Long>products,@Param("curdate")Date date);
    
	@Query(nativeQuery = true, value = OtherVariable.QUERY_2)
	Page<Promotion> findAlal(@Param("id") String id,@Param("pId") String pId,Pageable pageable);
}


