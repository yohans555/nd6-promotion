package com.nexsoft.app.repo;



import com.nexsoft.app.model.Promo.PromoFreeGood;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PromoFreeGoodRepository extends JpaRepository<PromoFreeGood, Integer> {
}


