package com.nexsoft.app.repo;

import java.util.ArrayList;

import com.nexsoft.app.Variable.OtherVariable;
import com.nexsoft.app.model.Transaction.Transaction;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;


public interface TransactionRepository extends JpaRepository<Transaction, String> {
    @Modifying(clearAutomatically = true)
	@Query(nativeQuery = true, value = "update transaction t set deleted=true where t.id= :id")
	void delete(@Param("id") String id);

    @Query(nativeQuery = true, value = "select * from transaction t where t.deleted=false")
	ArrayList<Transaction> find();

	@Query(nativeQuery = true, value = OtherVariable.QUERY_3)
	Page<Transaction> findAll(@Param("id") String id,Pageable pageable);
}