package com.nexsoft.app.repo;


import com.nexsoft.app.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;



public interface Productrepository extends JpaRepository<Product, Integer> {
    @Query(nativeQuery = true, value = "select * from product p where p.product_id=(:id) and p.name=(:name)")
    Product findByProductId(@Param("id")Long id,@Param("name")String name);
}


