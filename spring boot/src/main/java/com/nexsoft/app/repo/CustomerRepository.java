package com.nexsoft.app.repo;

import com.nexsoft.app.model.*;

import org.springframework.data.jpa.repository.JpaRepository;


public interface CustomerRepository extends JpaRepository<Customer, Long> {
}


