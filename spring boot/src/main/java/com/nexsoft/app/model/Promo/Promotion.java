package com.nexsoft.app.model.Promo;


import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.*;

import com.nexsoft.app.model.CustomIdGenerator;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="promotion")
public class Promotion {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "customId_sql")
    @GenericGenerator(name = "customId_sql",strategy ="com.nexsoft.app.model.CustomIdGenerator",parameters ={
        @Parameter(name= CustomIdGenerator.INCREMENT_PARAM,value  ="1"),
        @Parameter(name=CustomIdGenerator.VALUE_PREFIX_PARAMETER,value = "PROMO_"),
        @Parameter(name=CustomIdGenerator.NUMBER_FORMAT_DEFAULT,value = "%05d")
    } )
    private String id;

    private String description;
    private long forPurchase;
    @OneToMany(cascade = CascadeType.ALL)
    private List<PromoProduct> promoProduct;
    private String promoCategory;
    private Date documentDate = Date.valueOf(LocalDate.now());
    private Date startDate;
    private Date endDate;
    private boolean deleted=false;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "promotion", fetch = FetchType.LAZY)
    private PromoDiscount promoDiscount;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "promotion", fetch = FetchType.LAZY)
    private PromoFreeGood promoFreeGood;
}
