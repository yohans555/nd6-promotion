package com.nexsoft.app.model.Transaction;


import java.sql.Date;
import java.util.List;
import javax.persistence.*;
import com.nexsoft.app.model.CustomIdGenerator;
import com.nexsoft.app.model.Customer;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="transaction")
public class Transaction {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "customId_sql")
    @GenericGenerator(name = "customId_sql",strategy ="com.nexsoft.app.model.CustomIdGenerator",parameters ={
        @Parameter(name= CustomIdGenerator.INCREMENT_PARAM,value  ="1"),
        @Parameter(name=CustomIdGenerator.VALUE_PREFIX_PARAMETER,value = "FAKTUR_"),
        @Parameter(name=CustomIdGenerator.NUMBER_FORMAT_DEFAULT,value = "%05d")
    } )
    private String id;

    private Date date;
    @OneToOne
    private Customer customer;
    private Long subTotal;
    private Long total;
    private Long discount;
    private long product_quantity;
    private long product_variant;
    @OneToMany(cascade = CascadeType.ALL)
    private List<TransactionProduct> transactionProduct;
    private boolean deleted=false;

}
