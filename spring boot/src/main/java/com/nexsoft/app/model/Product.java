package com.nexsoft.app.model;

import javax.persistence.*;

@Entity
@Table(name="product")
public class Product {
    
    @Id
    private long productId;

    private String name;
    private long price;
    private long stock;

    public Product(){};
    public Product(long productId, String name, long price, long stock) {
        this.productId = productId;
        this.name = name;
        this.price = price;
        this.stock = stock;
    }
    public long getProductId() {
        return productId;
    }
    public void setProductId(long productId) {
        this.productId = productId;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public long getPrice() {
        return price;
    }
    public void setPrice(long price) {
        this.price = price;
    }
    public long getStock() {
        return stock;
    }
    public void setStock(long stock) {
        this.stock = stock;
    }

    


  
}
