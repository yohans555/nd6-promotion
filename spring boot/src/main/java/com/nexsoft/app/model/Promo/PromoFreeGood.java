package com.nexsoft.app.model.Promo;

import javax.persistence.*;

import com.nexsoft.app.model.Product;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="promofreegood")
public class PromoFreeGood {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private long quantity;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "product_Id")
    private Product product;
    @OneToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    @JoinColumn(name = "promotion_id")
    private Promotion promotion;

    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public long getQuantity() {
        return quantity;
    }
    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }
    public Product getProduct() {
        return product;
    }
    public void setProduct(Product product) {
        this.product = product;
    }
    public void setPromotion(Promotion promotion) {
        this.promotion = promotion;
    }
}
