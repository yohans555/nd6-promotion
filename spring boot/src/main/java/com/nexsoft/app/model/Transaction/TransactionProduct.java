package com.nexsoft.app.model.Transaction;

import javax.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="transactionproduct")
public class TransactionProduct {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private long productId;
    private String name;
    private long quantity;
    private long price;
    private long discount=0;
}
