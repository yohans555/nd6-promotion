package com.nexsoft.app.Controller;

import java.util.List;
import com.nexsoft.app.DTO.*;
import com.nexsoft.app.Variable.OtherVariable;
import com.nexsoft.app.model.*;
import com.nexsoft.app.model.Promo.*;
import com.nexsoft.app.model.Transaction.*;
import com.nexsoft.app.service.ModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/get")
@CrossOrigin(origins = OtherVariable.CROSS_ORIGIN)
public class GetController {

    @Autowired
    ModelService modelService;

    // Get mapping untuk berbagai entitas
    @GetMapping(value = "/promotion")
    public List<Promotion> getPromoPageAble(@RequestParam(name = "page") int page,@RequestParam(name = "elements") int elements,@RequestParam(name = "id") String id,@RequestParam(name = "pId") String pId){
        Pageable pageable = PageRequest.of(page, elements);
        return modelService.getAllPromotion(id, pId,pageable);
    }

    @GetMapping(value = "/transaction")
    public List<Transaction> getTransactionPageAble(@RequestParam(name = "page") int page,@RequestParam(name = "elements") int elements,@RequestParam(name = "id") String id){
        Pageable pageable = PageRequest.of(page, elements);
        return modelService.getAllTransaction(id,pageable); 
    }

    @GetMapping(value = "/customer")
    public List<Customer> getCustomer(){
        return modelService.getAllCustomer(); 
    }

    @GetMapping(value = "/product")
    public List<Product> getProduct(){
        return modelService.getAllProduct(); 
    }

    //Generate invoice note: promo is applied too
    @PostMapping(value = "/invoice", consumes = OtherVariable.CONSUME_JSON)
    public Transaction getInvoice(@RequestBody NewTransactionDTO nTransactionDTO){
        return modelService.getInvoice(nTransactionDTO); 
    }

    //Get The best promo based on product in cart
    @PostMapping(value = "/finalpromo", consumes = OtherVariable.CONSUME_JSON)
    public Promotion getFinalPromoId(@RequestBody NewTransactionDTO nTransactionDTO){
        return modelService.getFinalPromo(nTransactionDTO.getCart()); 
    }
}