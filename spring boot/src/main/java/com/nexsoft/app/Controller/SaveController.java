package com.nexsoft.app.Controller;

import com.nexsoft.app.DTO.NewPromoDTO;
import com.nexsoft.app.DTO.NewTransactionDTO;
import com.nexsoft.app.DTO.Response;
import com.nexsoft.app.Variable.OtherVariable;
import com.nexsoft.app.Variable.ResponseVariable;
import com.nexsoft.app.model.User;
import com.nexsoft.app.model.Promo.Promotion;
import com.nexsoft.app.service.ModelService;
import com.nexsoft.app.service.UserService;
import com.nexsoft.app.service.Validator.EditPromoValidator;
import com.nexsoft.app.service.Validator.PromoValidator;
import com.nexsoft.app.service.Validator.TransactionValidator;
import com.nexsoft.app.service.Validator.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
@RequestMapping(value = "/save")
@CrossOrigin(origins = OtherVariable.CROSS_ORIGIN)
public class SaveController {
    @Autowired
    ModelService modelService;
    @Autowired
    UserService userService;
    @Autowired
    PromoValidator promoValidator;
    @Autowired
    UserValidator userValidator;
    @Autowired
    TransactionValidator transactionValidator;
    @Autowired
    EditPromoValidator editPromoValidator;

    
    //to save new promotion
    @PostMapping(value = "/promotion", consumes = OtherVariable.CONSUME_JSON)
    public Response savePromo(@RequestBody NewPromoDTO newPromoDTO, BindingResult bindingResult){
        try {
            promoValidator.promoValidator(newPromoDTO, bindingResult);
            Response response = Response.ResponseFactory(bindingResult);
            if (!bindingResult.hasErrors()) {
                modelService.savePromo(newPromoDTO);
            } 
            return response;
        } catch (Exception e) {
            return new Response(ResponseVariable.STATUS_500, e.toString());
            }
    }
    
    // To edit existing promo
    @PostMapping(value = "/edit-promotion", consumes = OtherVariable.CONSUME_JSON)
    public Response saveEditPromo(@RequestBody Promotion promotion, BindingResult bindingResult){
        try {
            editPromoValidator.editPromoValidator(promotion, bindingResult);
            Response response = Response.ResponseFactory(bindingResult);
            if (!bindingResult.hasErrors()) {
                modelService.editPromotion(promotion);
            }
            return response;
       } catch (Exception e) {
            return new Response(ResponseVariable.STATUS_500, e.toString());
       } 
    }

    // Save New transaction
    @PostMapping(value = "/transaction", consumes = OtherVariable.CONSUME_JSON)
    public Response saveTransaction(@RequestBody NewTransactionDTO transaction, BindingResult bindingResult){
        try {
            transactionValidator.validate(transaction, bindingResult);
            Response response = Response.ResponseFactory(bindingResult);
            if (!bindingResult.hasErrors()) {
                modelService.saveTransaction(transaction);
            } 
            return response;
        }catch (Exception e) {
            return new Response(ResponseVariable.STATUS_500, e.getMessage());
            }       
    }

    // TO edit Existing Transaction
        @PostMapping(value = "/edit-transaction/{id}", consumes =OtherVariable.CONSUME_JSON)
        public Response editTransaction(@PathVariable("id") String id,@RequestBody NewTransactionDTO transaction, BindingResult bindingResult){
            try {
                transactionValidator.validate(transaction, bindingResult);
                Response response = Response.ResponseFactory(bindingResult);
                if (!bindingResult.hasErrors()) {
                    modelService.editTransaction(transaction,id);               
                }  
                return response;  
            } catch (Exception e) {
                return new Response(ResponseVariable.STATUS_500, e.getMessage());
            }
        }
        
    // Login 
    @PostMapping(value = "/login", consumes = OtherVariable.CONSUME_JSON)
    public Response Login(@RequestBody User user){
        User result= userService.getUserByNameAndPassword(user.getUsername(), user.getPassword());
        if (result == null) {
            return new Response(ResponseVariable.STATUS_404 ,"User Not found");
        } else {
            return new Response(ResponseVariable.STATUS_200, ResponseVariable.MSG_200);
        }
    }

    // Create New User
    @PostMapping(value = "/user", consumes =OtherVariable.CONSUME_JSON)
    public Response saveUser(@RequestBody User user, BindingResult bindingResult){
        try {
           userValidator.validate(user, bindingResult);
           Response response = Response.ResponseFactory(bindingResult);
            if (!bindingResult.hasErrors()) {
                userService.Saveuser(user);
            }
            return response;   
        } catch (Exception e) {
            return new Response(ResponseVariable.STATUS_500, e.getMessage());
        }
    }         
}