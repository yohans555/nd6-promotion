package com.nexsoft.app.Controller;

import com.nexsoft.app.Variable.OtherVariable;
import com.nexsoft.app.service.ModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping(value = "/delete")
@CrossOrigin(origins = OtherVariable.CROSS_ORIGIN)
public class DeleteController {
    @Autowired
    ModelService modelService;

    @DeleteMapping(value = "/promotion/{id}")
    public void deletePromo(@PathVariable("id")String id){
        modelService.deletePromo(id);
    }

    @DeleteMapping(value = "/transaction/{id}")
    public void deleteTransaction(@PathVariable("id")String id){
        modelService.deleteTransaction(id);
    }
}
