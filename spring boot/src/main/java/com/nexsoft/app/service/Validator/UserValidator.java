package com.nexsoft.app.service.Validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.nexsoft.app.Variable.ResponseVariable;
import com.nexsoft.app.model.User;
import com.nexsoft.app.repo.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class UserValidator implements Validator {
    @Autowired
    private UserRepository userRepository;

    @Override
    public boolean supports(Class<?> aClass) {
        return User.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        User user = (User) o;

        //validate password lenghth
        if (user.getPassword().length() < 6 || user.getPassword().length() > 12) {
            errors.rejectValue("password", "Size.userForm.password");
        }

         //Validating password with regex
         //Regular Expression for password
         String regex= "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*](?=\\S+$).{6,12}$" ; 
         //Compile regular expression to get the pattern  
         Pattern pattern = Pattern.compile(regex); 
         Matcher matcher = pattern.matcher(user.getPassword());
         //Throw error if email isnt match
         if (matcher.matches()==false){
             errors.rejectValue("password",ResponseVariable.INVALID_PATTERN+ "password");
         }

         //Validating Username with regex
        //  Regular Expression for Username
         String regexUsername= "^(?=[a-zA-Z0-9._]{8,20}$)(?!.*[_.]{2})[^_.].*[^_.]$" ; 
         //Compile regular expression to get the pattern  
         Pattern patternUsername = Pattern.compile(regexUsername); 
         Matcher matcherUsername = patternUsername.matcher(user.getUsername());
         //Throw error if email isnt match
         if (matcherUsername.matches()==false){
             errors.rejectValue("username", ResponseVariable.INVALID_PATTERN+"Username");
         }

         //Throw error if username already exist
         if (userRepository.findByUsername(user.getUsername())!=null){
            errors.rejectValue("Username", "Username Already Exist");
        }

    }
}
