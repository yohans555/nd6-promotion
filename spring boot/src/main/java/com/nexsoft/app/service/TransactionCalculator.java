package com.nexsoft.app.service;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import com.nexsoft.app.model.Promo.PromoDiscount;
import com.nexsoft.app.model.Promo.PromoFreeGood;
import com.nexsoft.app.model.Promo.PromoProduct;
import com.nexsoft.app.model.Promo.Promotion;
import com.nexsoft.app.model.Transaction.TransactionProduct;
import com.nexsoft.app.repo.PromoDiscountRepository;
import com.nexsoft.app.repo.PromoFreeGoodRepository;
import com.nexsoft.app.repo.PromotionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TransactionCalculator {
    @Autowired
    ModelService modelService;
    @Autowired
    PromotionRepository promotionRepository;
    @Autowired
    PromoDiscountRepository promoDiscountRepository;
    @Autowired
    PromoFreeGoodRepository promoFreeGoodRepository;

    //mencari list promo yang memenuhi syarat pembelian
    public List<Promotion> promoFinderSatisfied(List<TransactionProduct>products){
        List<Promotion> possiblePromo =promoFinderPossible(products);
        //checking the value of transaction promo product
        List<Promotion> satisfiedPromo = new ArrayList<>();
        for (Promotion promotion : possiblePromo) {
            long value = countPromoProductTransaction(promotion, products);
            if (value >= promotion.getForPurchase()){
                satisfiedPromo.add(promotion);
            }
        }
        return satisfiedPromo;
        
    }

    //mencari list promo yang mungkin didapat pembeli berdasarkan produk yg ia beli
    public List<Promotion> promoFinderPossible(List<TransactionProduct>products){
        //get list of id belanjaan
        ArrayList<Long> listProductsId = new ArrayList<>();{
            for (TransactionProduct element : products) {
                listProductsId.add(element.getProductId());
            }
        }
        //Search for all possible promos affiliated to the items customer buy
        List<Promotion> possiblePromo =promotionRepository.findAffiliatePromotions(listProductsId,Date.valueOf(LocalDate.now()));
        return possiblePromo;
    }

    //mencari promo yg akan diterapkan di pembelajaan
    public Promotion getPromoFinal(List<Promotion> satisfPromotions){
        List<Promotion> filteredList = satisfPromotions.stream()
            .filter(res->res.getPromoCategory().equalsIgnoreCase("freegood")).collect(Collectors.toList());
        
        if (filteredList.size()==0) {
            if (satisfPromotions.size()==0) {
                return new Promotion();
            } else {
                return optimumPromoDiscount(satisfPromotions);
            }
        }else return optimumPromoFreeGood(filteredList);
    }

    //menghitung subtotal (harga sebelum promo)
    public Long getSubTotalTransaction(List<TransactionProduct>products){
        long subTotal= 0;
        for (TransactionProduct transactionProduct : products) {
            subTotal=subTotal+(transactionProduct.getPrice()*transactionProduct.getQuantity());
        }
        return subTotal;
    }

    //menghitung toal biaya untuk transaksi
    public HashMap<String, Long> countTotal(List<TransactionProduct>products){
        Long subTotal = getSubTotalTransaction(products);
        Long discount= countLineDiscount(products);
        Long total = subTotal-discount;
        HashMap<String,Long> payment = new HashMap<>();{
            payment.put("subTotal", subTotal);
            payment.put("discount", discount);
            payment.put("total", total);
        }
        return payment;
    }

    // menghitung diskon yg didapat
    public Long countLineDiscount(List<TransactionProduct>products){
        Promotion promotion = getPromoFinal(promoFinderSatisfied(products));
        float lineDiscount = 0;
        if(promotion.getPromoCategory()!=null){
            if(promotion.getPromoCategory().equalsIgnoreCase("discount")){
                float discount = (float)promotion.getPromoDiscount().getDiscount();
                if(discount <=80){
                    List<TransactionProduct> filteredList = products.stream()
                    .filter(res -> promotion.getPromoProduct().stream()
                    .anyMatch(result-> res.getProductId()==result.getProductId()))
                    .collect(Collectors.toList());
                    for (TransactionProduct tProduct : filteredList) {
                        lineDiscount = lineDiscount + tProduct.getPrice()*tProduct.getQuantity()*(discount/100);
                        tProduct.setDiscount((long) discount);
                }
                }else{
                    lineDiscount=discount;
                }
            }
        }
        return (long) lineDiscount;
    }

    // menhitung value transaksi dari produk yang dikenai promo
    public long countPromoProductTransaction(Promotion promotion,List<TransactionProduct>products){
            long subTotalWithoutPromo = 0;
            for (PromoProduct promoProduct : promotion.getPromoProduct()) {
                for (TransactionProduct product : products) {
                    if (product.getProductId()==promoProduct.getProductId()) {
                        subTotalWithoutPromo=subTotalWithoutPromo+(product.getPrice()*product.getQuantity());
                    }
                }
                
            }
            return subTotalWithoutPromo;
    }

    // mencari promo dengna value diskon tertinggi untuk categori diskon
    public Promotion optimumPromoDiscount( List<Promotion> promoDiscount){
        Integer index=-1;
        long tempDisc=0;

        List<Promotion> filteredList = promoDiscount.stream()
            .filter(res->res.getPromoDiscount().getDiscount()<=80).collect(Collectors.toList());
        if(filteredList.size()!=0){
            promoDiscount=filteredList;
        }
        for (int i = 0; i < promoDiscount.size(); i++) {
            PromoDiscount tempDC = promoDiscount.get(i).getPromoDiscount();
            if (tempDC.getDiscount()>tempDisc) {
                index=i;
                tempDisc=tempDC.getDiscount();
            }
            if (tempDC.getDiscount()==tempDisc) {
                if(promoDiscount.get(i).getStartDate().before(promoDiscount.get(index).getStartDate())){
                    index=i;
                    tempDisc=tempDC.getDiscount();
                }               
            }
        }
        return promoDiscount.get(index);
    }

    // mencari promo dengan value free good tertinggi untuk kategori freeGOod
    public Promotion optimumPromoFreeGood(List<Promotion> promoFreeGood){
        Integer index = 0;
        long tempDisc = 0;
        for (int i = 0; i < promoFreeGood.size(); i++) {
            PromoFreeGood tempPFG = promoFreeGood.get(i).getPromoFreeGood();
            if (tempPFG.getQuantity()*tempPFG.getProduct().getPrice()>=tempDisc) {
                index=i;
                tempDisc=tempPFG.getQuantity()*tempPFG.getProduct().getPrice();
            }
        }
        return promoFreeGood.get(index);
    }
    
    // mencari jenis produk
    public Long countProductQuantity(List<TransactionProduct>cart){
        Long qty=(long)0;
        for (TransactionProduct transactionProduct : cart) {
            qty = qty + transactionProduct.getQuantity();
        }
        return  qty;
    }

}
