package com.nexsoft.app.service.Validator;

import java.sql.Date;
import java.time.LocalDate;
import com.nexsoft.app.DTO.NewPromoDTO;
import com.nexsoft.app.Variable.ResponseVariable;
import com.nexsoft.app.model.Promo.PromoProduct;
import com.nexsoft.app.repo.Productrepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class PromoValidator implements Validator {

    @Autowired Productrepository productrepository;

    public void promoValidator(Object o, Errors errors){
        if (supports(o.getClass())) {
            validate(o, errors);
        } else {
            errors.rejectValue("o", ResponseVariable.CLASS_EXCEPTION);
        }
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return NewPromoDTO.class.equals(aClass);
    }
    @Override
    public void validate(Object o, Errors errors) {
        NewPromoDTO newPromoDTO = (NewPromoDTO) o; 
        Date curDate = Date.valueOf(LocalDate.now());

        // Reject empty fields
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "description", ResponseVariable.EMPTY_FIELD+ " description");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "promoCategory", ResponseVariable.EMPTY_FIELD+ "promoCategory");
        
        // Reject Product that do not exist!
        for (PromoProduct p : newPromoDTO.getPromoProduct()) {
            if (productrepository.findByProductId(p.getProductId(),p.getName())==null) {
                errors.rejectValue("promoProduct", ResponseVariable.ITEM_NOT_FOUND);
            }  
        }

        //Checking for the Date input
        try {
            if (curDate.after((Date) newPromoDTO.getStartDate())) {
                errors.rejectValue("startDate", "Start Date At least Started today");
            }
            if (newPromoDTO.getStartDate().after((Date) newPromoDTO.getEndDate())) {
                errors.rejectValue("endDate", "End Date must be after Start Date");
            }
        } catch (NullPointerException e) {
            errors.rejectValue("endDate", ResponseVariable.NULL_POINTER+"Date");
        }
        

         // Check for promo restriction
         try {
            switch (newPromoDTO.getPromoCategory()) {
                case "discount":
                   if (newPromoDTO.getDiscount()> newPromoDTO.getForPurchase()){
                       errors.rejectValue("discount", "Discount must be lower than forPurchase");
                   }
                   if (newPromoDTO.getDiscount()<= 0){
                       errors.rejectValue("discount", "Discount must be Higher than 0");
                   }
                    break;
            
                default:
                   // Reject Product that do not exist!
                    long id = newPromoDTO.getFreeGood().getProductId();
                    String name = newPromoDTO.getFreeGood().getName();
                    if (productrepository.findByProductId(id,name)==null) {
                       errors.rejectValue("freeGood", ResponseVariable.ITEM_NOT_FOUND);
                    }
                    if(newPromoDTO.getQuantity()>newPromoDTO.getFreeGood().getStock()){
                       errors.rejectValue("quantity", ResponseVariable.INSUFFICIENT_STOCK+newPromoDTO.getFreeGood().getName());
                   }
                   if(newPromoDTO.getQuantity()<=0){
                    errors.rejectValue("quantity", ResponseVariable.NOT_EMPTY +"quantity");
                }
                    break;
            }
         } catch (NullPointerException e) {
                errors.rejectValue("promoCategory", ResponseVariable.NULL_POINTER+e );
         }
         
    }
}
