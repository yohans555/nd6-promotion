package com.nexsoft.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import com.nexsoft.app.model.User;
import com.nexsoft.app.repo.UserRepository;


@Service
@Transactional
public class UserService {
    @Autowired
    private UserRepository userRepository;

    // get all data from repo method
    public User getUserByNameAndPassword(String Username,String Password){
        return userRepository.findByUsernameAndPassword(Username,Password);
    }

    // Save Registration Form
    public void Saveuser(User user){
            userRepository.save(user);
        }
    }
