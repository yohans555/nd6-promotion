package com.nexsoft.app.service.Validator;

import java.sql.Date;
import java.time.LocalDate;

import com.nexsoft.app.Variable.ResponseVariable;
import com.nexsoft.app.model.Promo.PromoProduct;
import com.nexsoft.app.model.Promo.Promotion;
import com.nexsoft.app.repo.Productrepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class EditPromoValidator implements Validator {

    @Autowired Productrepository productrepository;
    
    public void editPromoValidator(Object o, Errors errors){
        if (supports(o.getClass())) {
            validate(o, errors);
        } else {
            errors.rejectValue("o", ResponseVariable.CLASS_EXCEPTION);
        }
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return Promotion.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        Promotion promotion = (Promotion) o; 

        Date curDate = Date.valueOf(LocalDate.now());
        Date startDate = (Date) promotion.getStartDate();
        Date endDate = (Date) promotion.getEndDate();

        // Reject empty fields
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "description", ResponseVariable.EMPTY_FIELD+ " description");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "promoCategory", ResponseVariable.EMPTY_FIELD+ "promoCategory");
        if (promotion.getPromoProduct().size()<1) {
            errors.rejectValue("promoProduct", ResponseVariable.NULL_POINTER+ " promoProduct");
        }

        // reject Product that do not exist
        for (PromoProduct p : promotion.getPromoProduct()) {
            if (productrepository.findByProductId(p.getProductId(),p.getName())==null) {
                errors.rejectValue("promoProduct", ResponseVariable.ITEM_NOT_FOUND);
            }  
        }

        //Checking for the Date input
        if (curDate.after(endDate)) {
            errors.rejectValue("startDate", "Edit Session Has expired");
        }
        if (startDate.after(endDate)) {
            errors.rejectValue("endDate", "End Date must be after Start Date");
        }

         // Check for promo restriction
         switch (promotion.getPromoCategory()) {
             case "discount":
                if (promotion.getPromoDiscount().getDiscount()> promotion.getForPurchase()){
                    errors.rejectValue("promoDiscount", "Discount must be lower than forPurchase");
                }
                 break;
         
             default:
                long stock =promotion.getPromoFreeGood().getProduct().getStock();
                long freeGoodQuantity = promotion.getPromoFreeGood().getQuantity();
                if(freeGoodQuantity>stock){
                    errors.rejectValue("promoFreeGood", ResponseVariable.INSUFFICIENT_STOCK);
                }
                 break;
         }
    }
}
