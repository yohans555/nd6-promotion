package com.nexsoft.app.service.Validator;


import com.nexsoft.app.DTO.NewTransactionDTO;
import com.nexsoft.app.Variable.ResponseVariable;
import com.nexsoft.app.model.Customer;
import com.nexsoft.app.model.Product;
import com.nexsoft.app.model.Transaction.Transaction;
import com.nexsoft.app.model.Transaction.TransactionProduct;
import com.nexsoft.app.repo.CustomerRepository;
import com.nexsoft.app.repo.Productrepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class TransactionValidator implements Validator { 
    @Autowired
    private Productrepository productrepository;
    @Autowired
    private CustomerRepository customerRepository;

    public void transactionValidator(Object o, Errors errors){
        if (supports(o.getClass())) {
            validate(o, errors);
        } else {
            errors.rejectValue("o", ResponseVariable.CLASS_EXCEPTION);
        }
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return Transaction.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        NewTransactionDTO transaction = (NewTransactionDTO) o;

        //validate Stock vs cart and throw exception if product do not exist and throw exception if cart empty
        try {
            if (transaction.getCart().size() <= 0) {
                errors.rejectValue("cart", ResponseVariable.NOT_EMPTY+"cart");
            }
            for (TransactionProduct tProduct : transaction.getCart()) {
            Product product =  productrepository.findByProductId(tProduct.getProductId(),tProduct.getName());
            if (product == null) {
                errors.rejectValue("cart", ResponseVariable.ITEM_NOT_FOUND+tProduct.getName());
            }else{
                Long stck = product.getStock();
                if (tProduct.getQuantity() > stck) {
                    errors.rejectValue("cart", ResponseVariable.INSUFFICIENT_STOCK+tProduct.getName());
                }
            }
            
        } 
        } catch (NullPointerException e) {
            errors.rejectValue("cart", ResponseVariable.NULL_POINTER+" transacton Product");
        }
       
         // make sure the Customer Exist
         try {
             Customer customer = customerRepository.findById(transaction.getCustomer().getId()).get();
             if (customer==null) {
                errors.rejectValue("customer", ResponseVariable.ITEM_NOT_FOUND+" Customer");
             }
         } catch (NullPointerException e) {
            errors.rejectValue("customer", ResponseVariable.NULL_POINTER+" Customer");
         }
    } 
}
