package com.nexsoft.app.service;

import java.sql.Date;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import javax.transaction.Transactional;
import com.nexsoft.app.DTO.*;
import com.nexsoft.app.model.*;
import com.nexsoft.app.model.Promo.PromoFreeGood;
import com.nexsoft.app.model.Promo.Promotion;
import com.nexsoft.app.model.Transaction.Transaction;
import com.nexsoft.app.model.Transaction.TransactionProduct;
import com.nexsoft.app.repo.*;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class ModelService {
    @Autowired
    private Productrepository productrepository;
    @Autowired
    private TransactionRepository transactionRepository;
    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private PromotionRepository promotionRepository;
    @Autowired
    private PromoFreeGoodRepository promoFreeGoodRepository;
    @Autowired
    private TransactionCalculator transactionCalculator;


    // get all data from repo method
    public List<Product> getAllProduct() {
        return productrepository.findAll();
    }

    public List<Customer> getAllCustomer() {
        return customerRepository.findAll();
    }

    public List<Transaction> getAllTransaction(String id, Pageable pageable) {
        return transactionRepository.findAll(id, pageable).getContent();
    }

    public List<Promotion> getAllPromotion(String id , String pId,Pageable pageable) {
        return promotionRepository.findAlal(id, pId, pageable).getContent();
    }

    // Save new Promo using newPromoDTO as transfer object
    public void savePromo(NewPromoDTO nPromoDTO) {
        ModelMapper mapper = new ModelMapper();
        Promotion promotion = mapper.map(nPromoDTO, Promotion.class);
        try {
            if (nPromoDTO.getPromoCategory().equalsIgnoreCase("discount")) {
                promotion.getPromoDiscount().setPromotion(promotion);
                promotion.setPromoFreeGood(null);
                promotionRepository.save(promotion);
            } else {
                promotion.setPromoDiscount(null);
                PromoFreeGood entity= promotion.getPromoFreeGood();
                entity.setPromotion(promotion);
                entity.setQuantity(nPromoDTO.getQuantity());
                promoFreeGoodRepository.save(entity);
                }   
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    // delete method to soft delete promotion from DB
    public void deletePromo(String id) {
        promotionRepository.delete(id);
    }

    public void deleteTransaction(String id) {
        transactionRepository.delete(id);
    }

    // get the best promo for transaction
    public Promotion getFinalPromo(List<TransactionProduct> transactionProducts) {
        return  transactionCalculator
                .getPromoFinal(transactionCalculator.promoFinderSatisfied(transactionProducts));
    }

    // get invoice before checkout
    public Transaction getInvoice(NewTransactionDTO nTransactionDTO) {
        HashMap<String, Long> data = transactionCalculator.countTotal(nTransactionDTO.getCart());
        Long product_variant = (long) nTransactionDTO.getCart().size();
        Long product_quantity = transactionCalculator.countProductQuantity(nTransactionDTO.getCart());
        Promotion finalPromo = transactionCalculator
                .getPromoFinal(transactionCalculator.promoFinderSatisfied(nTransactionDTO.getCart()));
        ;
        if (finalPromo.getPromoCategory() != null) {
            if (!finalPromo.getPromoCategory().equalsIgnoreCase("discount")) {
                Product freeProduct = finalPromo.getPromoFreeGood().getProduct();
                TransactionProduct freeGood = new TransactionProduct(0, freeProduct.getProductId(),
                        freeProduct.getName(), finalPromo.getPromoFreeGood().getQuantity(), 0,0);
                nTransactionDTO.getCart().add(freeGood);
            }
        }
        Transaction transaction = new Transaction();
        {
            transaction.setCustomer(nTransactionDTO.getCustomer());
            transaction.setDate(Date.valueOf(LocalDate.now()));
            transaction.setTransactionProduct(nTransactionDTO.getCart());
            transaction.setDiscount(data.get("discount"));
            transaction.setSubTotal(data.get("subTotal"));
            transaction.setTotal(data.get("total"));
            transaction.setProduct_variant(product_variant);
            transaction.setProduct_quantity(product_quantity);
        }
        return transaction;
    }

    // submit transaction
    public void saveTransaction(NewTransactionDTO transactionDTO) {
        Transaction transaction = getInvoice(transactionDTO);
        transactionRepository.save(transaction);    
    }

    // Edit transaction
    public void editTransaction(NewTransactionDTO transactionDTO,String id) {
        if(Date.valueOf(LocalDate.now()).compareTo(transactionRepository.findById(id).get().getDate()) != 0){
            throw new IllegalArgumentException("Edit Session Has expired");
        }
        Transaction transaction = getInvoice(transactionDTO);
        transaction.setId(id);
        transactionRepository.save(transaction);    
    }

    // Edit Promotion
    public void editPromotion(Promotion promotion) {
            switch (promotion.getPromoCategory()) {
                case "discount":
                    promotion.getPromoDiscount().setPromotion(promotion);
                    promotionRepository.save(promotion); 
                    break;
                case "freegood":
                    promotion.getPromoFreeGood().setPromotion(promotion);
                    promotionRepository.save(promotion); 
                    break;
                default:
                    break;
            }
    }
}