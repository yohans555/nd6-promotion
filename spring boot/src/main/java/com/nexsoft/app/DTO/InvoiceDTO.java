package com.nexsoft.app.DTO;
import java.util.Date;
import java.util.List;

import com.nexsoft.app.model.Product;
import com.nexsoft.app.model.Transaction.TransactionProduct;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class InvoiceDTO {
    private List<TransactionProduct> transactionProduct;
    private long subTotal;
    private String promoCategory;
    private Date startDate;
    private Date endDate;
    private String description;
    private Product freeGood;
    private long quantity;
    private long discount;
}
