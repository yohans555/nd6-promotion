package com.nexsoft.app.DTO;
import java.sql.Date;
import java.util.List;

import com.nexsoft.app.model.Product;
import com.nexsoft.app.model.Promo.PromoProduct;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class NewPromoDTO {
    private List<PromoProduct> promoProduct;
    private long forPurchase; 
    private String promoCategory; // not null
    private Date startDate; // not null
    private Date endDate; // not null
    private String description; // no empty or null
    private Product freeGood; //not null
    private long quantity;
    private long discount;
}
