package com.nexsoft.app.DTO;

import java.util.List;

import com.nexsoft.app.model.Customer;
import com.nexsoft.app.model.Transaction.TransactionProduct;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class NewTransactionDTO {
    private List<TransactionProduct> cart;
    private Customer customer;
    
}
