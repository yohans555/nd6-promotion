package com.nexsoft.app.DTO;


import com.nexsoft.app.Variable.ResponseVariable;

import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Response {
    private String status;
    private String messages;

    public static Response ResponseFactory(BindingResult bindingResult){
        Response response = new Response();
        if (bindingResult.hasErrors()) {
            String errors = "ERROR :";
            for (Object object : bindingResult.getAllErrors()) {
                if (object instanceof FieldError) {
                    FieldError fieldError = (FieldError) object;
                    errors = errors + fieldError.getCode() + "-";
                }
            }
            response.setStatus(ResponseVariable.STATUS_400);
            response.setMessages(errors);
        } else {
            response.setStatus(ResponseVariable.STATUS_200);
            response.setMessages(ResponseVariable.MSG_200);
        } 
        return response;
    }
}
