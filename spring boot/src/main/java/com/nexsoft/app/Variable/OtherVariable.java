package com.nexsoft.app.Variable;

public class OtherVariable {
    public static final String CROSS_ORIGIN = "http://localhost:3000";
    public static final String CONSUME_JSON = "application/JSON";
    public static final String QUERY_1= 
    "SELECT *  FROM promotion p " 
    +"inner join promotion_promo_product ppp  on p.id =ppp.promotion_id " 
    +"inner join promoproduct p2 on p2.id = ppp .promo_product_id " 
    +"where "
    +"p2.product_id in (:products) and "
    +"start_date <=(:curdate) and (:curdate)<=end_date "
    +"and p.deleted =false " 
    +"group by p.id;";

    public static final String QUERY_2= 
    "SELECT *  FROM promotion p " 
    +"inner join promotion_promo_product ppp  on p.id =ppp.promotion_id " 
    +"inner join promoproduct p2 on p2.id = ppp .promo_product_id " 
    +"where "
    +"p.id LIKE CONCAT('%',:id,'%') and "
    +"cast(p2.product_id as character(50)) LIKE CONCAT('%', :pId, '%') and "
    +"p.deleted =false " 
    +"group by p.id";

    public static final String QUERY_3= 
    "SELECT *  FROM transaction p " 
    +"where "
    +"p.id LIKE CONCAT('%',:id,'%') and "
    +"p.deleted =false ";
}
