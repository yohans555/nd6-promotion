package com.nexsoft.app.Variable;

public class ResponseVariable {
    public static String STATUS_200 = "STATUS 200 OK";
    public static String MSG_200 = "TRANSACTION SUCCESS";
    public static String STATUS_500 = "STATUS 500 INTERNAL ERROR: ";
    public static String STATUS_400 = "STATUS 400 BAD REQUEST: ";
    public static String STATUS_404 = "STATUS 404 NOT FOUND: ";

    public static String ID_NOT_NULL =  "Id cannot be null";
    public static String NOT_EMPTY =  "This Field Can Not be Empty : ";
    public static String INSUFFICIENT_STOCK =  "Stock is insufficient :";
    public static String ILLEGAL_ARGUMENT =  "Illegal Argument for field : ";
    public static String EMPTY_FIELD =  "This Field Can not be Empty";
    public static String ITEM_NOT_FOUND =  "Item Not Found :";
    public static String NULL_POINTER = "Null Pointer Exception";
    public static String CLASS_EXCEPTION = "This API not supported with provided requestbody";
    public static String INVALID_PATTERN = "Pattern is Invalid : ";
}
